package action;
import constants.Constants;
import entity.Sphere;
import static constants.Constants.N3;
import static constants.Constants.SPHERE_AREA_CONST;
import static constants.Constants.VOLUME_CONST;

/**
 * computations on sphere.
 */
public final class Computation {

    /**
     * private constructor.
     */
    private Computation() {
    }
    /**
     * @param radius radius of sphere
     * @return sphere area
     */
    public static double computeSphereArea(final double radius) {
        return Math.round(SPHERE_AREA_CONST * Math.pow(radius, 2));
    }

    /**
     * @param radius sphere radius
     * @return sphere volume
     */
    public static double computeSphereVolume(final double radius) {
            return Math.round(VOLUME_CONST * Math.pow(radius, N3));

    }

    /**
     * @param height is how much you want to subtract
     * from the top of the sphere to compute volume ratio.
     * @param radius radius of sphere
     * @return volume ratio
     */

    public static double computeVolumeRatio(final double radius,
                                            final double height) {
            double volume = VOLUME_CONST * Math.pow(radius, N3);
            double volume1 = Constants.RATIO_CONST * Math.pow(height, 2)
                    * (N3 * radius - height);
            double volume2 = volume - volume1;
            return round(volume1 / volume2);
    }

    /**
     * @param sphere current sphere.
     * @return  true, if sphere not refers to coordinate plane .
     */
    public static boolean isRefersCoordinatePlane(final Sphere sphere) {
        return sphere.getRadius() >= sphere.getCenter().getDotX()
                || sphere.getRadius() >= sphere.getCenter().getDotY()
                || sphere.getRadius() >= sphere.getCenter().getDotZ();
    }

    /**
     * @param sphere object sphere
     * @return sphere is object if radius > 0
     */
    public static boolean isSphere(final Sphere sphere) {
        return sphere.getRadius() > 0;
    }

    /**
     * @param value number.
     * @return number with 2 signs after dot
     */
    private static double round(final double value) {
        final int s = 10;
        final int places = 4;
        long factor = (long) Math.pow(s, places);
        double temp =  value;
        temp = temp * factor;
        long tmp = Math.round(temp);
        return (double) tmp / factor;
    }
}
