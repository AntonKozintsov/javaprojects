package test;

import entity.Dot;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * to test Dot class.
 */
public class DotTest {
    /**
     * @return dot object
     */
    @DataProvider(name = "dotObj")
    public static Object[][] dotObject() {
        return new Object[][]{{new Dot(9, 9, 9), new Dot(1, 1, 1), false},
                {new Dot(9, 9, 9), new Dot(9, 9, 9), true}};
    }

    /**
     * @param dot1 dot1 object
     * @param dot2 dot2 object
     * @param expectedResult correct equals
     */
    @Test(dataProvider = "dotObj")
    public void testEquals(final Dot dot1, final Dot dot2,
                           final boolean expectedResult) {
        Assert.assertEquals(dot1.equals(dot2), expectedResult);
    }

    /**
     * @param dot1 dot1 object
     * @param dot2 dot2 object
     * @param expectedResult correct equals
     */
    @Test(dataProvider = "dotObj")
    public void testHashCode(final Dot dot1, final Dot dot2,
                             final boolean expectedResult) {
        Assert.assertEquals(dot1.hashCode() == dot2.hashCode(), expectedResult);
    }
}
