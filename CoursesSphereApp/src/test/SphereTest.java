package test;

import entity.Dot;
import entity.Sphere;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * to test sphere class.
 */
public class SphereTest {
    /**
     * @return sphere objects
     **/
    @DataProvider(name = "sphereObj")
    public static Object[][] sphereObj() {
        return new Object[][]{{new Sphere(10, new Dot(5, 5, 5)),
                new Sphere(10, new Dot(5, 5, 5)), true},
                {new Sphere(15, new Dot(5, 5, 5)),
                        new Sphere(5, new Dot(5, 5, 5)), false}};
    }

    /**
     * @param sphere1 sphere object1
     * @param sphere2 sphere object2
     * @param expectedResult correct result
     */
    @Test(dataProvider = "sphereObj")
    public void testEquals(final Sphere sphere1, final Sphere sphere2,
                           final boolean expectedResult) {
        Assert.assertEquals(sphere1.equals(sphere2), expectedResult);
    }

    /**
     * @param sphere1 sphere object1
     * @param sphere2 sphere object2
     * @param expectedResult correct result
     */
    @Test(dataProvider = "sphereObj")
    public void testHashCode(final Sphere sphere1, final Sphere sphere2,
                             final boolean expectedResult) {
        Assert.assertEquals(sphere1.hashCode()
                == sphere2.hashCode(), expectedResult);
    }
}
