package test;

import entity.Dot;
import entity.Sphere;
import exception.StreamException;
import fileaction.Parser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.ArrayList;
import static constants.Constants.FILE_NAME;

/**
 * to test parser method. If you add more strings to test file, it will fails
 * to test it correct strings in file must be the same as in dataProvider.
 * use data/test1.txt[in CONSTANTS class] if you want to test all lines, but
 * without ParserTest
 */
public class ParserTest {
    /**
     * @return list of sphere objects.
     */
    @DataProvider(name = "linesFromFile")
    public static Object[][] lines() {
        Dot dot1 = new Dot(9, 9, 9);
        Sphere sphere1 = new Sphere(100, dot1);
        ArrayList<Sphere> list = new ArrayList<>();
        list.add(sphere1);
        return new Object[][]{{list}};
    }

    /**
     * @param sphere sphere object
     * @throws StreamException exception
     */
    @Test(dataProvider = "linesFromFile")
    public void testObjectList(final ArrayList<Sphere> sphere)
            throws StreamException {
        Assert.assertEquals(sphere, Parser.objectList(FILE_NAME));
    }

}
