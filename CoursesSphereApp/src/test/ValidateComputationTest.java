package test;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import validation.ValidateComputation;

/**
 * to test validations.
 */
public class ValidateComputationTest {
    /**
     * @return data to test validations{radius, height,
     * expected result(true or false}
     */
    @DataProvider(name = "validate")
    public Object[][] validateData() {
        return new Object[][]{
                {5, 10, true}, {Double.MAX_VALUE, 10, false},
                {0, 0, false}, {10, Double.MAX_VALUE, false}};
    }

    /**
     * @param radius sphere radius
     * @param height height to compute ratio
     * @param expectedResult correct result
     */
    @Test(dataProvider = "validate")
    public void testValidate(final double radius, final double height,
                             final boolean expectedResult) {
        Assert.assertEquals(ValidateComputation.
                validate(radius, height), expectedResult);
    }
}
