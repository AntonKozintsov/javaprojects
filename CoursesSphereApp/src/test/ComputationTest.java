package test;

import action.Computation;
import constants.Constants;
import entity.Dot;
import entity.Sphere;
import exception.StreamException;
import fileaction.Parser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static constants.Constants.FILE_NAME_CHECK;
import static constants.Constants.HEIGHT;

/**
 * class for computation tests.
 */
public class ComputationTest {

    /**
     * determine logger to log computations.
     */
    private static final Logger LOG =
            LogManager.getLogger(ComputationTest.class.getName());

    /**
     * @return data to test sphere area computations
     */
    @DataProvider(name = "area")
    public Object[][] area() {
        return new Object[][]{
                {2, 50}, {5, 314}, {0, 0}};
    }

    /**
     * @return data to test sphere volume computations
     */
    @DataProvider(name = "volume")
    public Object[][] volume() {
        return new Object[][]{
                {2, 34}, {5, 524}, {0, 0}};
    }

    /**
     * @return data to test sphere volume ration computations
     */
    @DataProvider(name = "volumeRatio")
    public Object[][] ratio() {
        return new Object[][]{
                {3, Constants.HEIGHT, 12.5}, {5, Constants.HEIGHT, 1}};
    }

    /**
     * @return return to test isSphere computations
     */
    @DataProvider(name = "isSphere")
    public static Object[][] isSphere() {
        Dot dot1 = new Dot(9, 9, 9);
        Sphere sphere1 = new Sphere(200, dot1);
        Sphere sphere2 = new Sphere(0, dot1);
        return new Object[][]{{sphere1, true}, {sphere2, false}};

    }

    /**
     * @return data to test equals and hashcode methods
     */
    @DataProvider(name = "sphereObj")
    public static Object[][] sphereObj() {
        Dot dot1 = new Dot(9, 9, 9);
        Sphere sphere1 = new Sphere(9, dot1);
        Sphere sphere2 = new Sphere(8, dot1);
        return new Object[][]{{sphere1, true}, {sphere2, false}};
    }

    /**
     * @param radius sphere radius
     * @param expectedResult is correct sphere area
     */
    @Test(dataProvider = "area")
    public void testComputeSphereArea(final double radius,
                                      final double expectedResult) {
        Assert.assertEquals(Computation.computeSphereArea(radius),
                expectedResult);
    }

    /**
     * @param radius sphere radius
     * @param expectedResult is correct sphere volume
     */
    @Test(dataProvider = "volume")
    public void testComputeSphereVolume(final double radius,
                                        final double expectedResult) {
        Assert.assertEquals(Computation.computeSphereVolume(radius),
                expectedResult);
    }

    /**
     * @param radius sphere radius
     * @param height to calculate ratio
     * @param expectedResult correct sphere ratio
     */
    @Test(dataProvider = "volumeRatio")
    public void testComputeVolumeRatio(final double radius, final double height,
                                       final double expectedResult) {
        Assert.assertEquals(Computation.computeVolumeRatio(radius, height),
                expectedResult);
    }

    /**
     * @param sphere correct sphere object
     * @param expectedResult correct result
     */
    @Test(dataProvider = "sphereObj")
    public void testIsRefersCoordinatePlane(final Sphere sphere,
                                            final boolean expectedResult) {
        Assert.assertEquals(Computation.isRefersCoordinatePlane(sphere),
                expectedResult);
    }

    /**
     * @param sphere correct sphere object
     * @param expectedResult correct result
     */
    @Test(dataProvider = "isSphere")
    public void testIsSphere(final Sphere sphere,
                             final boolean expectedResult) {
        Assert.assertEquals(Computation.isSphere(sphere), expectedResult);
    }

    /**
     * @throws StreamException exception
     */
    @Test
    public void testComputeData() throws StreamException {
        //test to show calculations from file test2.txt
        ArrayList<Sphere> sphereList = new ArrayList<Sphere>();
        sphereList = Parser.objectList(FILE_NAME_CHECK);
        for (Sphere aList : sphereList) {
            LOG.info("sphere area: "
                    + Computation.computeSphereArea((aList.getRadius())));
            LOG.info("sphere volume: "
                    + Computation.computeSphereVolume(aList.getRadius()));
            LOG.info("volume ration: "
                    + Computation.computeVolumeRatio(aList.getRadius(),
                    HEIGHT));
            LOG.info("is sphere refers to coordinate plane: "
                    + Computation.isRefersCoordinatePlane(aList));
            LOG.info("isSphere" + Computation.isSphere(aList));
        }
    }
}
