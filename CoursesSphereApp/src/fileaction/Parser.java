package fileaction;

import constants.Constants;
import entity.Dot;
import entity.Sphere;
import exception.StreamException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import validation.ValidateComputation;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Stream;
/**
 * Class to perform actions with data from file.
 */
public final class Parser {
    /**
     * Logger.
     */
    private static final Logger LOG_ERROR = LogManager.getLogger("ERROR");
    /**
     * constructor.
     */
    private Parser() {
    }

    /**
     * @param fileName name of file with data
     * @return double [] array from file
     * @throws StreamException file exception
     */
    private static double[] readFromFile(final String fileName)
            throws StreamException {
        double[] numbers;
        try {
            numbers = Files.lines(Paths.get(fileName))
                    .filter(line -> line.matches(Constants.PATTERN))
                    .flatMap(line -> Stream.of(line.split(" ")))
                    .mapToDouble(Double::parseDouble)
                    .toArray();
        } catch (IOException e) {
             throw new StreamException("problems with file");
        }
        return numbers;
    }

    /**
     *
     * @param fileName name of file with data
     * @return correct list of sphere objects
     * @throws StreamException if file not exit
     */
    public static ArrayList<Sphere> objectList(final String fileName)
            throws StreamException {
        ArrayList<Sphere> list = new ArrayList<>();
        double[] numbers = readFromFile(fileName);

        for (int i = 0; i < Objects.requireNonNull(numbers).length;
             i = i + Constants.N4) {
            //if data is valid we put it in list of objects
            if (ValidateComputation.validate(numbers[i], Constants.HEIGHT)) {
                Dot dot = new Dot(numbers[i + 1],
                        numbers[i + 2], numbers[i + Constants.N3]);
                Sphere sphere = new Sphere(numbers[i], dot);
                list.add(sphere);
            } else {
                LOG_ERROR.error("radius is incorrect" + numbers[i]);
            }
        }
        return list;
    }
}
