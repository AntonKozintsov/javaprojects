package validation;

import static constants.Constants.N3;
import static constants.Constants.SPHERE_AREA_CONST;
import static constants.Constants.VOLUME_CONST;
import static constants.Constants.MAX_VALUE;
/**
 * validation class.
 */
public final class ValidateComputation {
    /**
     * private constructor.
     */
    private ValidateComputation() {

    }
    /**
     * @param radius sphere radius
     * @return true if radius is valid for area
     */
    private static boolean validateSphereArea(final double radius) {
        return (SPHERE_AREA_CONST * radius < MAX_VALUE);

    }

    /**
     * @param radius of sphere
     * @return true if radius is valid for volume
     */
    private static boolean validateSphereVolume(final double radius) {
        return (VOLUME_CONST * Math.pow(radius, N3)) < MAX_VALUE;

    }

    /**
     * @param radius radius of sphere
     * @param height is how much you want to subtract from diameter
     * to calculate volume ratio
     * @return true if radius and height are valid
     */
    private static boolean validateVolumeRatio(final double radius,
                                               final double height) {
        return ((height > 0 && (height <= 2 * radius)));

    }

    /**
     * @param radius radius of sphere
     * @param height height of sphere
     * @return true if all parameters are valid
     */
    public static boolean validate(final double radius, final double height) {
        return  (validateSphereArea(radius)
                && validateSphereVolume(radius)
                && validateVolumeRatio(radius, height));
    }
}
