package entity;

import java.util.Objects;

/**
 * sphere class.
 */
public class Sphere {
    /**
     * dot consist of 3 coordinates(x,y,z).
     */
    private Dot center;
    /**
     * radius of sphere.
     */
    private double radius;

    /**
     * @param mRadius radius of sphere.
     * @param mCenter center of sphere.
     */
    public Sphere(final double mRadius, final Dot mCenter) {
        this.radius = mRadius;
        this.center = mCenter;
    }

    /**
     * @return radius of sphere.
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @return center of sphere.
     */
    public Dot getCenter() {
        return center;
    }

    /**
      * @return to string.
     **/
    @Override
    public String toString() {
        return "Sphere{"
                + "center=" + center
                + ", radius=" + radius
                + '}';
    }

    /**
     * @param o object param.
     * @return true if equals.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sphere sphere = (Sphere) o;
        return Double.compare(sphere.getRadius(), getRadius()) == 0
                && Objects.equals(getCenter(), sphere.getCenter());
    }

    /**
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getCenter(), getRadius());
    }
}
