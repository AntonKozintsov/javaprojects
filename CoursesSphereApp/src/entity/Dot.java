package entity;

import java.util.Objects;

/**
 *
 */
public class Dot {
    /**
     * coordinate x.
     */
    private double dotX;
    /**
     * coordinate y.
     */
    private double dotY;
    /**
     * coordinate z.
     */
    private double dotZ;

    /**
     *
     * @param mDotX coordinate X.
     * @param mDotY coordinate Y.
     * @param mDotZ coordinate Z.
     */
    public Dot(final double mDotX, final double mDotY, final double mDotZ) {
        this.dotX = mDotX;
        this.dotY = mDotY;
        this.dotZ = mDotZ;
    }

    /**
     * @return coordinate X
     */
    public double getDotX() {
        return dotX;
    }

    /**
     * @return coordinate Y
     */
    public double getDotY() {
        return dotY;
    }

    /**
     * @return coordinate Z
     */
    public double getDotZ() {
        return dotZ;
    }

    /**
     * @return
     */
    @Override
    public String toString() {
        return "Dot{"
                + "dotX=" + dotX
                + ", dotY=" + dotY
                + ", dotZ=" + dotZ
                + '}';
    }

    /**
     * equals method.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dot dot = (Dot) o;
        return Double.compare(dot.getDotX(), getDotX()) == 0
                && Double.compare(dot.getDotY(), getDotY()) == 0
                && Double.compare(dot.getDotZ(), getDotZ()) == 0;
    }

    /**
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getDotX(), getDotY(), getDotZ());
    }
}
