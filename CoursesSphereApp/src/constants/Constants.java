package constants;

/**
 * class for constants.
 */
public final class Constants {
    /**
     * private constructor.
     */
    private Constants() {
    }
    /**
     * FILENAME constant for name of the file for Parser test class.
     */
    public static final String FILE_NAME = "data/test.txt";
    /**
     * FILE_NAME_CHECK for test string validations and logs.
     */
    public static final String FILE_NAME_CHECK = "data/test2.txt";
    /**
     * PATTERN is regular expression to read strings from the file.
     */
    public static final String PATTERN =
            "^(\\d+\\.?(\\d+)?\\s){3}\\d+\\.?(\\d+)?$";
    /**
     * VOLUME_CONST to compute volume of the sphere.
     */
    public static final double VOLUME_CONST = 4.0 / 3.0 * Math.PI;
    /**
     * RATIO_CONST to compute ratio.
     */
    public static final double RATIO_CONST = (1.0 /  3.0) * Math.PI;
    /**
     * SPHERE_AREA_CONST to compute sphere area.
     */
    public static final double SPHERE_AREA_CONST = 4 * Math.PI;
    /**
     * N3 to avoid magic numbers.
     */
    public static final int N3 = 3;
    /**
     * N4 to avoid magic numbers.
     */
    public static final int N4 = 4;
    /**
     * HEIGHT is how much you want to subtract
     * from the top of the sphere to compute volume ratio.
     */
    public static final double HEIGHT = 5;
    /**
     * Double max value.
     */
    public static final double MAX_VALUE = Double.MAX_VALUE;
}
