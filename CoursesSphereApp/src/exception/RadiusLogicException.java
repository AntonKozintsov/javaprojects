package exception;

/**
 * Custom exception for incorrect radius.
 */
public class RadiusLogicException extends Exception {
    /**
     *
     */
    public RadiusLogicException() {
    }

    /**
     *
     * @param message for message
     */
    public RadiusLogicException(final String message) {
        super(message);
    }

    /**
     *
     * @param exception for exception
     */
    public RadiusLogicException(final Throwable exception) {
        super(exception);
    }

    /**
     *
     * @param message message
     * @param exception exception
     */
    public RadiusLogicException(final String message,
                                final Throwable exception) {
        super(message, exception);
    }

}
