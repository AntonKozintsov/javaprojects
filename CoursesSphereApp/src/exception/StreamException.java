package exception;

/**
 * Class for exception in stream.
 * */
public class StreamException extends Exception {

    /**
     * constructor.
     */
    public StreamException() {
        super();
    }

    /**
     *
     * @param message message
     */
    public StreamException(final String message) {
        super(message);
    }

    /**
     *
     * @param exception exception
     */
    public StreamException(final Throwable exception) {
        super(exception);
    }
    /**
     *
     * @param message message
     * @param exception exception
     */
    public StreamException(final String message, final Throwable exception) {
        super(message, exception);
    }

}

