-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL COMMENT 'user id\\n',
  `login` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'login for account',
  `password` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'password for account',
  `role` tinyint(2) NOT NULL COMMENT 'role(admin, user, trainer)',
  `name` varchar(50) COLLATE utf8_bin NOT NULL COMMENT 'name',
  `pasport` varchar(9) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_bin DEFAULT NULL COMMENT 'phone',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User table. This table store information about user account, admin account and trainer account.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'MaximDlc','943f943',0,'Maxim','MP9345483','375295649302'),(1,'EgorMinsk','4141',0,'Egor',NULL,'375443594341'),(2,'CoolName','123456',0,'Pavel','MP1234567','375294354312'),(3,'Trainer_Andrew','1111af',1,'Andrew','MP945384','375258495324'),(4,'Trainer_Nikita','2341fk',1,'Nikita','MP1234569','375291232424'),(5,'Trainer_Anton','1928nma',1,'Anton','MP2839292','375294734747'),(6,'Admin_Anastasiya','mypass1',2,'Anastasiya','MP3598492','375294358434'),(7,'Admin_Ekaterina','accpass3f',2,'Ekaterina','MP1939292','375295848483'),(8,'Nickname','pdj84394',0,'Alexander','Mp5844343','375296834288'),(9,'MarsMan','1234az1234',0,'Artur','MP3845324','375448534343'),(10,'Markul','mc3434lp',0,'Boris','MP2342434','375295342434');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-29 20:12:52
