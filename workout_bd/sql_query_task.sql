-- 4.1) Показать id, логин и имя пользователей, чьи id > 5.
SELECT id, login, name
 FROM mydb.user
 WHERE id >5; 
 
 -- 4.2) Покаать Показать id, группу здоровья и скидку пользователей, у которых сумма денег от 300 до 1000.
SELECT id, discount, cash
 FROM mydb.client
 WHERE cash BETWEEN 300 and 1000;
 
-- 5.1) Показать длину пароля пользователя.
SELECT id, LENGTH(password)
 FROM mydb.user;

-- 5.2) Показать время в формате UNIX формате.
SELECT id, UNIX_TIMESTAMP(birth_date)
 FROM mydb.client;


-- 6.1) Показать какому клиенту принадлежит заказ. 
SELECT  mydb.order.id, mydb.client.id, mydb.order.cost
 FROM mydb.order
 INNER JOIN mydb.client ON mydb.order.client_id=mydb.client.id;


-- 7.1) Показать клиентов сгруппированных по группе здоровья и скидке.
SELECT * FROM mydb.client
GROUP BY mydb.health_group, mydb.client.discount
HAVING mydb.client.discount=0;


-- 7.2) Показать сколько клиентов у каждого тренера.
SELECT COUNT(client_id), trainer_id
 FROM mydb.order
 GROUP BY mydb.order.trainer_id;

 
 -- 8.1) Объединить id из таблицы Training и training_id из таблицы Task.
SELECT id 
 FROM mydb.training
 UNION ALL
SELECT training_id FROM mydb.task
 ORDER BY id;
 
-- 9.1) Взаимосвязный запрос. Показать всех клиентов у кого есть скидка.
 SELECT id, discount, cash
 FROM mydb.client
 WHERE discount IN
(SELECT discount 
 FROM mydb.client
 WHERE discount!=0);

-- 9.2) Несвязный запрос. Показать логин и пароль всех клиентов.
SELECT id, login, password 
 FROM mydb.user
 WHERE id IN
 (SELECT id 
  FROM mydb.client);
