package com.epam.by.action;

import com.epam.by.entity.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.by.constants.Constant.RUBLE_DOLLAR_RATE;
import static com.epam.by.constants.Constant.RUBLE_EURO_RATE;

/**
 * Class to perform exchange between 2 clients.
 */
public final class ExchangeUtil {
    /**
     * message if deal is succeed.
     */
    private static final String SUCCESS_DEAL =
            "Deal is succeed between clients: ";
    /**
     *
     */
    private static final String FAILED_DEAL =
            "Deal cannot be finished between clients: ";
    /**
     * To log information.
     */
    private static final Logger LOG = LogManager
            .getLogger(ExchangeUtil.class.getName());

    /**
     * private constructor.
     */
    private ExchangeUtil() {
    }

    /**
     * @param buyingClient  client who want to buy BEL_RUBLE for DOLLAR
     * @param sellingClient client who want to sell DOLLAR for BEL_RUBLE
     * @param amount        of BEL_RUBLE to exchange
     */
    public static void buyRubleForDollar(final Client buyingClient,
                                         final Client sellingClient,
                                         final int amount) {

        if (sellingClient.getBelRuble() > amount
                && buyingClient.getDollar() > amount * RUBLE_DOLLAR_RATE) {

            buyingClient.setBelRuble(buyingClient.getBelRuble() + amount);
            buyingClient.setDollar(buyingClient.getDollar()
                    - Math.round(amount * RUBLE_DOLLAR_RATE));
            sellingClient.setBelRuble(sellingClient.getBelRuble() - amount);
            sellingClient.setDollar(sellingClient.getDollar()
                    + Math.round(amount * RUBLE_DOLLAR_RATE));
            System.out.println("Client: " + buyingClient.getClientId()
                    + " bought "
                    + amount + " BEL_RUBLE for DOLLAR from "
                    + "Client:" + sellingClient.getClientId());
            System.out.print(buyingClient);
            System.out.println(sellingClient);
        } else {
            dealMessage(buyingClient, sellingClient);
        }
    }

    /**
     * @param buyingClient  client who want to buy BEL_RUBLE for EURO
     * @param sellingClient client who want to sell EURO for BEL_RUBLE
     * @param amount        of BEL_RUBLE to exchange
     */
    public static void buyRubleForEuro(final Client buyingClient,
                                       final Client sellingClient,
                                       final int amount) {
        if (sellingClient.getBelRuble() > amount
                && buyingClient.getEuro() > amount * RUBLE_EURO_RATE) {
            buyingClient.setBelRuble(buyingClient.getBelRuble() + amount);
            buyingClient.setEuro(buyingClient.getEuro()
                    - Math.round(amount * RUBLE_EURO_RATE));
            sellingClient.setBelRuble(sellingClient.getBelRuble() - amount);
            sellingClient.setEuro(sellingClient.getEuro()
                    + Math.round(amount * RUBLE_EURO_RATE));
            System.out.println("Client:" + buyingClient.getClientId()
                    + " bought " + amount + " BEL_RUBLE for EURO from"
                    + "Client:" + sellingClient.getClientId());
            System.out.print(buyingClient);
            System.out.println(sellingClient);
        } else {
          dealMessage(buyingClient, sellingClient);
        }
    }

    /**
     * @param buyingClient client who buy
     * @param sellingClient client who sell
     */
    private static void dealMessage(final Client buyingClient,
                                    final Client sellingClient) {
        if (LOG.isInfoEnabled()) {
            LOG.info(SUCCESS_DEAL + buyingClient.getClientId()
                    + sellingClient.getClientId());
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info(FAILED_DEAL + buyingClient.getClientId()
                        + sellingClient.getClientId());
            }
            System.out.println("deal between client:"
                    + buyingClient.getClientId() + " and client:"
                    + sellingClient.getClientId() + " cannot be finished");
        }
    }

}
