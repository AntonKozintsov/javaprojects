package com.epam.by.action;

import com.epam.by.entity.Client;
import com.epam.by.entity.StockExchange;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * deal class to make a deal between 2 clients.
 */
public class StockDeal implements Callable<String> {
    /**
     * number one.
     */
    private static final int MONEY_TO_EURO = 1;
    /**
     * number zero.
     */
    private static final int MONEY_TO_DOLLAR = 0;
    /**
     * client object.
     */
    private final transient Client client;
    /**
     * deal type can be bel ruble to dollar or bel ruble to euro.
     */
    private final transient int dealType;
    /**
     * stock exchange.
     */
    private final transient StockExchange stockExchange =
            StockExchange.getInstance();

    /**
     * @param client client
     * @param dealType deal type
     */
    public StockDeal(final Client client, final int dealType) {
        this.client = client;
        this.dealType = dealType;
    }

    /**
     * @return client
     */
    @Override
    public String call() {
        if (this.dealType == MONEY_TO_DOLLAR) {
            final int moneyToDollar = GenerateNumber.generateMoneyAmount();
            System.out.println("Client" + client.getClientId()
                    + ": want to trade" + moneyToDollar + " $ ");
            stockExchange.completeDeal(client, moneyToDollar, 0);
        } else if (this.dealType == MONEY_TO_EURO) {
            final int moneyToEuro = GenerateNumber.generateMoneyAmount();
            System.out.println("Client" + client.getClientId()
                    + ": want to trade" + moneyToEuro + " E ");
            stockExchange.completeDeal(client, moneyToEuro, 1);
        }
        try {
            TimeUnit.MILLISECONDS.sleep(2000);
        } catch (InterruptedException ignored) {
        }
        return client.toString();
    }
}
