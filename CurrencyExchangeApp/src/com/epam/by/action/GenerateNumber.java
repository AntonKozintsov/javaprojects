package com.epam.by.action;

import java.util.Random;

/**
 * Class to generate Numbers.
 */
public final class GenerateNumber {
    /**
     * Random value.
     */
    private static final Random RANDOM = new Random();
    /**
     * private constructor.
     */
    private GenerateNumber() {
    }
    /**
     * @return deal amount that can be 1 or 2
     */
    public static int generateDealAmount() {
        return RANDOM.nextBoolean() ? 1 : 2;
    }

    /**
     * @return random value between 0-200
     */
    public static int generateMoneyAmount() {
        return RANDOM.nextInt(200);
    }

    /**
     * @return deal type that can be 0 or 1(buy dollar or euro)
     */
    public static int generateDealType() {
        return RANDOM.nextBoolean() ? 0 : 1;
    }
}
