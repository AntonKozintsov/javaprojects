package com.epam.by.action;

/**
 * class to generate ID.
 */
public final class GenerateId { //NOPMD
    /**
     * id counter.
     */
    private static int idCounter;

    /**
     * private constructor.
     */
    private GenerateId() {
    }

    /**
     * @return increment id
     */
    public static int createID() {
        return idCounter++;
    }
}
