package com.epam.by.constants;

/**
 * Class for constants.
 */
public final class Constant { //NOPMD

    /**
     * Ruble to dollar exchange rate.
     */
    public static final double RUBLE_DOLLAR_RATE = 0.5;
    /**
     * Ruble to euro exchange rate.
     */
    public static final double RUBLE_EURO_RATE = 0.43;
    /**
     * Dollar to ruble exchange rate.
     */
    public static final double DOLLAR_RUBLE_RATE = 2;
    /**
     * Euro to ruble exchange rate.
     */
    public static final double EURO_RUBLE_RATE = 3;
    /**
     * FILENAME constant for name of the file for testing.
     */
    public static final String FILE_NAME = "data/test.txt";
    /**
     * Pattern to read only valid lines from file.
     */
    public static final String FILE_PATTERN =
            "^(\\d+\\.?(\\d+)?\\s){2}\\d+\\.?(\\d+)?$";

    /**
     * private constructor.
     */
    private Constant() {
    }
}
