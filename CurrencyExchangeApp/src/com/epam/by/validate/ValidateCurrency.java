package com.epam.by.validate;

import com.epam.by.constants.Constant;
import com.epam.by.entity.Client;

/**
 * class to validate currency amount.
 */
public final class ValidateCurrency {
    /**
     * private constructor.
     */
    private ValidateCurrency() {
    }

    /**
     * @param client client
     * @param amount amount of Bel Ruble
     * @param dealType type of deal
     * @return false if data is incorrect
     */
    public static boolean checkCurrency(final Client client,
                                        final int amount, final int dealType) {
        return checkDollar(client, amount)
                && checkEuro(client, amount);
    }

    /**
     * @param client client
     * @param amount currency amount
     * @return true if data is correct
     */
    private static boolean checkDollar(final Client client,
                                       final int amount) {
        return client.getBelRuble() > amount * Constant.DOLLAR_RUBLE_RATE;

    }

    /**
     * @param client client
     * @param amount currency
     * @return true if data is correct
     */
    private static boolean checkEuro(final Client client,
                                       final int amount) {
        return client.getBelRuble() > amount * Constant.EURO_RUBLE_RATE;

    }
}
