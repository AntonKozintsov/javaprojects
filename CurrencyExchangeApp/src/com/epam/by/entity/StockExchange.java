package com.epam.by.entity;

import com.epam.by.action.ExchangeUtil;
import com.epam.by.validate.ValidateCurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * thread safe singleton stock exchange.
 */
public final class StockExchange {
    /**
     * instance.
     */
    private static StockExchange instance = null;
    /**
     * reentrant lock.
     */
    private static ReentrantLock lock = new ReentrantLock();
    /**
     * client list.
     */
    private List<Client> clientList = new ArrayList<>();

    /**
     * private constructor.
     */
    private StockExchange() {
    }

    /**
     * @return instance
     */
    public static StockExchange getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new StockExchange();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    /**
     * @return array list of clients.
     */
    public List<Client> getClientList() {
        return clientList;
    }

    /**
     * @param clientList list of clients
     */
    public void setClientList(final List<Client> clientList) {
        this.clientList = clientList;
    }

    /**
     * @param client client
     * @param money amount
     * @param dealType type of deal
     */
    public void completeDeal(final Client client, final int money,
                             final int dealType) {
        try {
            lock.lock();
            final List<Client> clientList = getClientList();
            if (ValidateCurrency.checkCurrency(client, money, 0)) {
                for (final Client client1 : clientList) {
                    if (client.equals(client1)) {
                        continue;
                    }
                    if (money <= 0) {
                        break;
                    }
                    if (dealType == 0) {
                        ExchangeUtil.buyRubleForDollar(client,
                                client1, money);
                    } else if (dealType == 1) {
                        ExchangeUtil.buyRubleForEuro(client, client1, money);
                    }
                    TimeUnit.MILLISECONDS.sleep(1500);

                }
            } else {
             System.out.println(client.getClientId()
                     + " have not enough currency");
            }
        } catch (InterruptedException ignored) {
        } finally {
            lock.unlock();
        }
    }
}
