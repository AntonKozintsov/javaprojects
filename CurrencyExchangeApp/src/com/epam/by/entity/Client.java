package com.epam.by.entity;

import com.epam.by.action.StockDeal;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Client entity class.
 */
public class Client implements Callable<List<Future<String>>> {
    /**
     * client clientId.
     */
    private final transient int clientId;
    /**
     * amount of Bel Ruble.
     */
    private double belRuble;
    /**
     * amount of Dollar.
     */
    private double dollar;
    /**
     * amount of Euro.
     */
    private double euro;
    /**
     * amount of Deals.
     */
    private final transient int dealAmount;
    /**
     * deal type(ruble to dollar or ruble to euro).
     */
    private final transient int dealType;
    /**
     * list of deals.
     */
    private transient List<StockDeal> dealList;

    /**
     *
     * @param clientId clientId
     * @param belRuble amount of Bel Ruble
     * @param dollar amount of Dollar
     * @param euro amount of Euro
     * @param dealAmount amount of deals
     * @param dealType deal type
     */
    public Client(final int clientId, final double belRuble,
                  final double dollar, final double euro,
                  final int dealAmount, final int dealType) {
        this.clientId = clientId;
        this.belRuble = belRuble;
        this.dollar = dollar;
        this.euro = euro;
        this.dealAmount = dealAmount;
        this.dealType = dealType;
    }

    /**
     * @return clientId
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * @return Bel Ruble
     */
    public double getBelRuble() {
        return belRuble;
    }

    /**
     * @param belRuble Bel Ruble
     */
    public void setBelRuble(final double belRuble) {
        this.belRuble = belRuble;
    }

    /**
     * @return Dollar
     */
    public double getDollar() {
        return dollar;
    }

    /**
     * @param dollar Dollar
     */
    public void setDollar(final double dollar) {
        this.dollar = dollar;
    }

    /**
     * @return Euro
     */
    public double getEuro() {
        return euro;
    }

    /**
     * @param euro Euro
     */
    public void setEuro(final double euro) {
        this.euro = euro;
    }

    /**
     * @return amount of deals
     */
    public int getDealAmount() {
        return dealAmount;
    }

    /**
     * @return deal type
     */
    public int getDealType() {
        return dealType;
    }

    /**
     * @param dealList List of deals
     */
    public void setDealList(final List<StockDeal> dealList) {
        this.dealList = dealList;
    }

    /**
     * @return result
     */
    @Override
    public List<Future<String>> call() {

        final ExecutorService service =
                Executors.newFixedThreadPool(dealAmount);
        final List<Future<String>> result = new ArrayList<>();

        final List<StockDeal> deals1 = this.dealList;
        for (final StockDeal deal : deals1) {
            result.add(service.submit(deal));
        }
        service.shutdown();

        return result;
    }

    /**
     * @return to string value
     */
    @Override
    public String toString() {
        return "Client{"
                + "clientId=" + clientId
                + ", belRuble=" + belRuble
                + ", dollar=" + dollar
                + ", euro=" + euro
                + ", dealAmount=" + dealAmount
                + '}' + "\n";
    }


    /**
     * @param object object
     * @return true if equals
     */
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Client)) {
            return false;
        }
        final Client client = (Client) object;
        return clientId == client.clientId
                && Double.compare(client.belRuble, belRuble) == 0
                && Double.compare(client.dollar, dollar) == 0
                && Double.compare(client.euro, euro) == 0
                && dealAmount == client.dealAmount
                && dealType == client.dealType
                && Objects.equals(dealList, client.dealList);
    }

    /**
     * @return hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(clientId, belRuble, dollar, euro,
                dealAmount, dealType, dealList);
    }


}
