package com.epam.by.fileaction;

import com.epam.by.action.GenerateId;
import com.epam.by.action.GenerateNumber;
import com.epam.by.action.StockDeal;
import com.epam.by.entity.Client;
import com.epam.by.entity.StockExchange;
import com.epam.by.exception.StreamException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * parse string and create objects.
 */
public final class CreateObjectList {
    /**
     * to log information.
     */
    private static final Logger LOG_INFO = LogManager.getLogger("ERROR");
    /**
     * private constructor.
     */
    private CreateObjectList() {
    }
    /**
     * @return list of client objects
     * @throws StreamException exception while using stream
     */
    private static List<Client> objectList()
            throws StreamException {
        final List<String> numbers = GetDataFromFile.readFromFile();
        final List<Client> list = new ArrayList<>();

        for (int i = 0; i < numbers.size(); i = i + 3) {
            final Client client = new Client(GenerateId.createID(),
                    Double.valueOf(numbers.get(i)),
                    Double.valueOf(numbers.get(i + 1)),
                    Double.valueOf(numbers.get(i + 2)),
                    GenerateNumber.generateDealAmount(),
                    GenerateNumber.generateDealType());
            final ArrayList<StockDeal> dealList = new ArrayList<>();
            for (int j = 0; j < client.getDealAmount(); j++) {
                final StockDeal deal =
                        new StockDeal(client, client.getDealType());
                dealList.add(deal);
            }
            list.add(client);
            client.setDealList(dealList);
            if (LOG_INFO.isInfoEnabled()) {
                LOG_INFO.info("client created successfully");
            }
        }
        return list;
    }

    /**
     * @throws StreamException exception while using stream.
     */
    public static void addObjectToStorage() throws StreamException {
        final StockExchange exchange = StockExchange.getInstance();
        exchange.setClientList(objectList());
    }
}
