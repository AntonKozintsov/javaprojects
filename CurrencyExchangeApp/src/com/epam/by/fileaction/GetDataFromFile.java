package com.epam.by.fileaction;

import com.epam.by.constants.Constant;
import com.epam.by.exception.StreamException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * parse lines from files to string.
 */
final class GetDataFromFile {
    /**
     * private constructor.
     */
    private GetDataFromFile() {
    }

    /**
     * @return String
     * @throws StreamException exception
     */
    public static List<String> readFromFile()
            throws StreamException {
        List<String> list;
        try {
            list = Files.lines(Paths.get(Constant.FILE_NAME))
                    .filter(line ->
                            line.matches(Constant.FILE_PATTERN))
                    .flatMap(line -> Stream.of(line.split(" ")))
                    .collect(Collectors.toList());
            return list;
        } catch (IOException e) {
            throw new StreamException("problems with file");
        }
    }
}
