package com.epam.by.runner;


import com.epam.by.entity.Client;
import com.epam.by.entity.StockExchange;
import com.epam.by.exception.StreamException;
import com.epam.by.fileaction.CreateObjectList;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to run program.
 */
public final class RunApplication {
    /**
     * private constructor.
     */
    private RunApplication() {
    }
    /**
     * @param args arguments
     * @throws StreamException stream exception
     */
    public static void main(final String[] args) throws StreamException {
        CreateObjectList.addObjectToStorage();
        final StockExchange stockExchange = StockExchange.getInstance();
        final List<Client> clientList = stockExchange.getClientList();
        final ExecutorService service = Executors.
                newFixedThreadPool(clientList.size());
        for (final Client client : clientList) {
            service.submit(client);
        }
        service.shutdown();
    }
}
