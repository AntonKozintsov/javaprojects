package com.epam.by.specification;

import com.epam.by.entity.Registrator;

import java.util.ArrayList;

public interface SphereSpecification {
    ArrayList<Registrator> specified();
}

