package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;

public class SortByCoordinate implements SphereSpecification, Comparator<Registrator> {
    private static final Logger LOG = LogManager.getLogger(SearchByArea.class.getName());
    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> list = new ArrayList<Registrator>(storage.getSphereObj().values());
        list.sort(new SortByCoordinate());
        LOG.info(list);
        return list;
    }
    @Override
    public int compare(Registrator o1, Registrator o2) {
        return (int) (o1.getSphere().getCenter().getDotX() - o2.getSphere().getCenter().getDotX());
    }
}
