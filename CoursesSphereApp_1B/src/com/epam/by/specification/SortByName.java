package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;

public class SortByName implements Comparator<Registrator>, SphereSpecification {

    private static final Logger LOG = LogManager.getLogger(SearchByArea.class.getName());

    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> list = new ArrayList<>(storage.getSphereObj().values());
        list.sort(new SortByName());
        LOG.info(list);
        return list;
    }
    @Override
    public int compare(Registrator o1, Registrator o2) {
        return o1.getSphere().getName().compareToIgnoreCase(o2.getSphere().getName());
    }
}
