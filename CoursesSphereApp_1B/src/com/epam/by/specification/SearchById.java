package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class SearchById implements SphereSpecification{

    private String id;
    private static final Logger LOG = LogManager.getLogger(SearchByArea.class.getName());
    public SearchById(String id) {
        this.id = id;
    }

    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> list = new ArrayList<>(storage.getSphereObj().values());

        ArrayList<Registrator> sortedList = new ArrayList<>();
        for (Registrator aList : list) {
            if (aList.getSphere().getId().equals(id)) {
                sortedList.add(aList);
            }
        }
        if (sortedList.isEmpty()) {
            LOG.error("no such id: " +id);
        }
        LOG.info(sortedList);
        return sortedList;
    }
}
