package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class SearchByCoordinate implements SphereSpecification {

    private double coordinateX;
    private static final Logger LOG = LogManager.getLogger(SearchByArea.class.getName());
    public SearchByCoordinate(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> list = new ArrayList<>(storage.getSphereObj().values());

        ArrayList<Registrator> sortedList = new ArrayList<>();
        for (Registrator aList : list) {
            if (aList.getSphere().getCenter().getDotX() == coordinateX) {
                sortedList.add(aList);
            }
        }
        if (sortedList.isEmpty()) {
            LOG.error("no such coordinate X: " + coordinateX);
        }
        LOG.info(sortedList);
        return sortedList;
    }

}
