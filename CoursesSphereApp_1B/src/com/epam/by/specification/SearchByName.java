package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class SearchByName implements SphereSpecification {

    private String name;
    private static final Logger LOG = LogManager.getLogger(SearchByArea.class.getName());
    public SearchByName(String name) {
        this.name = name;
    }

    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> sortedList = new ArrayList<>();
        for (int i = 0; i < storage.getSphereObj().size(); i++) {
            if (storage.getSphereObj().get(Integer.toString(i)) != null &&
                    storage.getSphereObj().get(Integer.toString(i)).getSphere().getName().equals(name)) {
                sortedList.add(storage.getSphereObj().get(Integer.toString(i)));
            }
        }
        if (sortedList.isEmpty()) {
            LOG.error("no such name: " +name);
        }
        LOG.info(sortedList);
        return sortedList;
    }
}
