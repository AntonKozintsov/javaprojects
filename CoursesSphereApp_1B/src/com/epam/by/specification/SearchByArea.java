package com.epam.by.specification;

import com.epam.by.entity.Registrator;
import com.epam.by.repository.SphereStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Search by area specification.
 */
public class SearchByArea implements SphereSpecification {
    /**
     * area to search.
     */
    private double area;
    /**
     * to log info.
     */
    private static final Logger LOG = LogManager.
            getLogger(SearchByArea.class.getName());

    /**
     * @param area to search
     */
    public SearchByArea(final double area) {
        this.area = area;

    }

    /**
     * @return searching results
     */
    @Override
    public ArrayList<Registrator> specified() {
        SphereStorage storage = SphereStorage.getInstance();
        ArrayList<Registrator> list = new ArrayList<>(
                storage.getSphereObj().values());

        ArrayList<Registrator> sortedList = new ArrayList<>();
        for (Registrator aList : list) {
            if (aList.getArea() == area) {
                sortedList.add(aList);
            }
        }
        if (sortedList.isEmpty()) {
            LOG.error("no such area: " + area);
        }
        LOG.info(sortedList);
        return sortedList;
    }
}
