package com.epam.by.test;

import com.epam.by.action.ComputationUtil;
import com.epam.by.entity.Dot;
import com.epam.by.entity.Registrator;
import com.epam.by.entity.Sphere;
import com.epam.by.observer.SphereObserver;
import com.epam.by.repository.SphereRepository;
import com.epam.by.repository.SphereStorage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;

public class SphereRepositoryTest {

    private static SphereStorage storage = SphereStorage.getInstance();
    @DataProvider(name = "testRepository")
    public static Object[][] repository() {
        storage.getSphereObj().clear();
        Dot dot = new Dot(5, 5, 5);
        Sphere sphere = new Sphere("5", "fifth", 5, dot);
        HashMap<String, Registrator> map = new HashMap<>();
        double area = ComputationUtil.computeSphereArea(sphere.getRadius());
        double volume = ComputationUtil.computeSphereVolume(sphere.getRadius());
        Registrator registrator = new Registrator(sphere, area, volume);
        map.put(sphere.getId(), registrator);
        return new Object[][]{{sphere, map}};
    }
    @DataProvider(name = "testChanging")
    public static Object[][] changeRepository() {
        SphereObserver observer = new SphereObserver();
        storage.getSphereObj().clear();
        Dot dot = new Dot(15, 15, 15);
        Sphere sphere = new Sphere("4", "fourth", 4, dot);
        Sphere sphere1 = new Sphere("4", "fourth", 25, dot);
        double area = ComputationUtil.computeSphereArea(sphere1.getRadius());
        double volume = ComputationUtil.computeSphereVolume(sphere1.getRadius());
        Registrator registrator1 = new Registrator(sphere, area, volume);;
        return new Object[][]{{sphere, 25, registrator1}};
    }
    @Test(dataProvider = "testRepository")
    public void testAddSphere(Sphere sphere, HashMap<String, Registrator> map) {
        SphereRepository repository = new SphereRepository();
        repository.addSphere(sphere);
        Assert.assertEquals(storage.getSphereObj().get("5").getSphere(), map.get("5").getSphere());
    }

    @Test(dataProvider = "testRepository")
    public void testRemoveSphere(Sphere sphere, HashMap<String, Registrator> map) {
        SphereRepository repository = new SphereRepository();
        repository.removeSphere(sphere);
        map.remove("5");
        Assert.assertEquals(storage.getSphereObj(), map);

    }

    @Test(dataProvider = "testChanging")
    public void testChangeSphere(Sphere sphere, double radius, Registrator registrator) {
        storage.addSphereToMap(sphere);
        SphereRepository repository = new SphereRepository();
        repository.changeSphere(sphere, radius);
        storage.getSphereObj().get(sphere.getId()).getSphere().changeRadius(radius);
        Assert.assertEquals(storage.getSphereObj().get("4").getArea(), registrator.getArea() );
    }
}