package com.epam.by.test;

import com.epam.by.entity.Dot;
import com.epam.by.entity.Sphere;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * to test entity Sphere.
 */
public class SphereTest {
    /**
     * @return sphere objects
     **/
    @DataProvider(name = "equalsAndHash")
    public static Object[][] sphereObj() {
        return new Object[][]{{new Sphere("1", "first", 5, new Dot(5, 5, 5)),
                new Sphere("1", "first", 5, new Dot(5, 5, 5)), true},
                {new Sphere("2", "second", 15, new Dot(10, 10, 10)),
                        new Sphere("2", "third", 15, new Dot(10, 10, 10)),
                        false}};
    }
    /**
     * @return objects to test changeradius method
     */
    @DataProvider(name = "testChangeRadius")
    public static Object[][] sphereObjTestRadius() {
        return new Object[][]
                {{new Sphere("1", "first", 5, new Dot(5, 5, 5)), 15},
                {new Sphere("2", "second", 25, new Dot(10, 10, 10)), 15}};
    }

    /**
     * @param sphere sphere object
     * @param expectedResult changed radius
     */
    @Test(dataProvider = "testChangeRadius")
    public void testChangeRadius(final Sphere sphere,
                                 final double expectedResult) {
        sphere.changeRadius(15);
        Assert.assertEquals(sphere.getRadius(), expectedResult);
    }
    /**
     * @param sphere1        sphere object1
     * @param sphere2        sphere object2
     * @param expectedResult correct result
     */
    @Test(dataProvider = "equalsAndHash")
    public void testEquals(final Sphere sphere1, final Sphere sphere2,
                           final boolean expectedResult) {
        Assert.assertEquals(sphere1.equals(sphere2), expectedResult);
    }
    /**
     * @param sphere1        sphere object1
     * @param sphere2        sphere object2
     * @param expectedResult correct result
     */
    @Test(dataProvider = "equalsAndHash")
    public void testHashCode(final Sphere sphere1, final Sphere sphere2,
                             final boolean expectedResult) {
        Assert.assertEquals(sphere1.hashCode()
                == sphere2.hashCode(), expectedResult);
    }
}
