package com.epam.by.test;

import com.epam.by.action.ComputationUtil;
import com.epam.by.entity.Dot;
import com.epam.by.entity.Registrator;
import com.epam.by.entity.Sphere;
import com.epam.by.repository.SphereStorage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;

public class SphereStorageTest {
    private SphereStorage storage = SphereStorage.getInstance();
    @DataProvider(name = "testStorage")
    public static Object[][] storage() {
        Dot dot = new Dot(5, 5, 5);
        Sphere sphere = new Sphere("1", "first", 5, dot);
        HashMap<String, Registrator> map = new HashMap<>();
        double area = ComputationUtil.computeSphereArea(sphere.getRadius());
        double volume = ComputationUtil.computeSphereVolume(sphere.getRadius());
        Registrator registrator = new Registrator(sphere, area, volume);
        map.put(sphere.getId(), registrator);
        return new Object[][]{{map}};
    }

    @Test(dataProvider = "testStorage")
    public void testAddSphereToMap(HashMap<String, Registrator> map) {
        Sphere sphere = new Sphere("1", "first", 5, new Dot(5, 5, 5));
        storage.addSphereToMap(sphere);
        Assert.assertEquals(storage.getSphereObj().get("0"), map.get("0"));

    }

    @Test(dataProvider = "testStorage")
    public void testRemoveSphereFromMap(HashMap<String, Registrator> map) {
        storage.removeSphereFromMap(storage.getSphereObj().get("1").getSphere());
        map.remove("1");
        Assert.assertEquals(storage.getSphereObj(), map);
    }
}