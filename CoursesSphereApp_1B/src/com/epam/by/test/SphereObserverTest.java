package com.epam.by.test;

import com.epam.by.entity.Dot;
import com.epam.by.entity.Sphere;
import com.epam.by.observer.SphereObserver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SphereObserverTest {
    private SphereObserver observer;
    @DataProvider(name = "testHandleEvent")
    public static Object[][] testEvent() {
        return new Object[][]
                {{new Sphere("1", "first", 5, new Dot(5, 5, 5)), 15},
                        {new Sphere("2", "second", 25, new Dot(10, 10, 10)), 15}};
    }

    @Test(dataProvider = "testHandleEvent")
    public void testHandleEvent(Sphere sphere)  {

    }
}