package com.epam.by.test;

import com.epam.by.action.ComputationUtil;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * To test action Computation class.
 */
public class ComputationUtilTest {
    /**
     * @return data to test sphere area calculations
     */
    @DataProvider(name = "area")
    public Object[][] area() {
        return new Object[][]{
                {2, 50}, {5, 314}, {0, 0}};
    }
    /**
     * @return data to test sphere volume calculations
     */
    @DataProvider(name = "volume")
    public Object[][] volume() {
        return new Object[][]{
                {2, 34}, {5, 524}, {0, 0}};
    }
    /**
     * @param radius sphere radius
     * @param expectedResult is correct sphere area
     */
    @Test(dataProvider = "area")
    public void testComputeSphereArea(final double radius,
                                      final double expectedResult) {
        Assert.assertEquals(ComputationUtil.computeSphereArea(radius),
                expectedResult);
    }
    /**
     * @param radius sphere radius
     * @param expectedResult is correct sphere volume
     */
    @Test(dataProvider = "volume")
    public void testComputeSphereVolume(final double radius,
                                        final double expectedResult) {
        Assert.assertEquals(ComputationUtil.computeSphereVolume(radius),
                expectedResult);
    }
}
