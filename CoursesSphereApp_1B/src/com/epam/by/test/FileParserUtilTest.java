package com.epam.by.test;

import com.epam.by.entity.Dot;
import com.epam.by.entity.Sphere;
import com.epam.by.exception.StreamException;
import com.epam.by.fileaction.FileParserUtil;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static com.epam.by.constants.ConstantsUtil.FILE_NAME;

/**
 * to test parser method. If you add more strings to test file, it will fails
 * to test it correct strings in file must be the same as in dataProvider.
 */
public class FileParserUtilTest {
    /**
     * @return list of sphere objects.
     */
    @DataProvider(name = "linesFromFile")
    public static Object[][] lines() {
        Dot dot1 = new Dot(1, 1, 1);
        Sphere sphere1 = new Sphere("0", "first", 1, dot1);
        ArrayList<Sphere> list = new ArrayList<>();
        list.add(sphere1);
        return new Object[][]{{list}};
    }

    /**
     * @throws StreamException exception
     */
    @Test(dataProvider = "linesFromFile")
    public void testObjectList(ArrayList<Sphere> sphere)
            throws StreamException {
        Assert.assertEquals(sphere,FileParserUtil.objectList(FILE_NAME));
    }
}