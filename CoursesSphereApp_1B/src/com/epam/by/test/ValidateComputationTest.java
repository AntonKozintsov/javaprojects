package com.epam.by.test;

import com.epam.by.validation.ValidateComputation;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * to test validations.
 */
public class ValidateComputationTest {
    /**
     * @return data to test validations{radius, height,
     * expected result(true or false}
     */
    @DataProvider(name = "validate")
    public Object[][] validateData() {
        return new Object[][]{
                {5, true}, {Double.MAX_VALUE, false},
                {0, false}, {Double.MIN_VALUE , true}};
    }

    /**
     * @param radius sphere radius
     * @param expectedResult correct result
     */
    @Test(dataProvider = "validate")
    public void testValidate(final double radius, final boolean expectedResult) {
        Assert.assertEquals(ValidateComputation.
                validate(radius), expectedResult);
    }
}
