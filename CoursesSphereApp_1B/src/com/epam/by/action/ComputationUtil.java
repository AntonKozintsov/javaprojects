package com.epam.by.action;


import static com.epam.by.constants.ConstantsUtil.SPHERE_AREA_CONST;
import static com.epam.by.constants.ConstantsUtil.VOLUME_CONST;

/**
 * computations on sphere.
 */
public final class ComputationUtil {

    /**
     * private constructor.
     */
    private ComputationUtil() {
    }
    /**
     * @param radius radius of sphere
     * @return sphere area
     */
    public static double computeSphereArea(final double radius) {
        return Math.round(SPHERE_AREA_CONST * Math.pow(radius, 2));
    }

    /**
     * @param radius sphere radius
     * @return sphere volume
     */
    public static double computeSphereVolume(final double radius) {
            return Math.round(VOLUME_CONST * Math.pow(radius, 3));

    }
}
