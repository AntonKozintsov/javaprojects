package com.epam.by.action;

/**
 * class to generate id.
 */
public final class GenerateIdUtil {
    /**
     * id counter.
     */
    private static int idCounter;
    /**
     * private constructor.
     */
    private GenerateIdUtil() {
    }
    /**
     * @return increment id
     */
    public static String  createID() {
        return String.valueOf(idCounter++);
    }
}
