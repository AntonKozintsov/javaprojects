package com.epam.by.fileaction;

import com.epam.by.action.GenerateIdUtil;
import com.epam.by.constants.ConstantsUtil;
import com.epam.by.entity.Dot;
import com.epam.by.entity.Sphere;
import com.epam.by.exception.StreamException;
import com.epam.by.repository.SphereRepository;
import com.epam.by.validation.ValidateComputation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.epam.by.constants.ConstantsUtil.FILE_NAME_CHECK;

/**
 * Class to perform actions with data from file.
 */
public final class FileParserUtil {
    /**
     * repository
     */
    private static SphereRepository repository = new SphereRepository();
    /**
     * Logger.
     */
    private static final Logger LOG_ERROR = LogManager.getLogger("ERROR");

    /**
     * constructor.
     */
    private FileParserUtil() {
    }

    /**
     * @param fileName name of file with data
     * @return double [] array from file
     * @throws StreamException file com.epam.by.exception
     */
    private static List<String> readFromFile(final String fileName)
            throws StreamException {
        List<String> list;
         try {
            list = Files.lines(Paths.get(fileName))
                    .filter(line -> line.matches(ConstantsUtil.PATTERN))
                    .flatMap(line -> Stream.of(line.split(" ")))
                    .collect(Collectors.toList());
            return list;
        } catch (IOException e) {
            throw (StreamException)new  StreamException("problems with file");
        }

    }

    /**
     * @param fileName name of file with data
     * @throws StreamException if file not exit
     */
    public static ArrayList<Sphere> objectList(final String fileName)
            throws StreamException {
        final List<String> numbers = readFromFile(fileName);
        ArrayList<Sphere> list = new ArrayList<>();
        for (int i = 0; i < numbers.size();
             i = i + 5) {
//            if data is valid we put it in list of objects
            if (ValidateComputation.validate(Double.valueOf(numbers.get(i + 1)))) {
                final Dot dot = new Dot(Double.valueOf(numbers.get(i + 2)),
                        Double.valueOf(numbers.get(i + 3)),
                        Double.valueOf(numbers.get(i + 4)));
                final Sphere sphere = new Sphere(GenerateIdUtil.createID(),
                        numbers.get(i),
                        Double.valueOf(numbers.get(i + 1)), dot);
                list.add(sphere);
            } else {
                LOG_ERROR.error("radius is incorrect" + numbers.get(i));
            }
        }
        return list;
    }

    public static void repository () throws StreamException {
        ArrayList<Sphere> list = objectList(FILE_NAME_CHECK);
        list.forEach(aList -> repository.addSphere(aList));
    }

}
