package com.epam.by.repository;

import com.epam.by.entity.Registrator;
import com.epam.by.entity.Sphere;
import com.epam.by.exception.RadiusLogicException;
import com.epam.by.specification.SphereSpecification;

import java.util.List;

/**
 * repository interface
 */
public interface Repository {
    /**
     * @param sphere
     */
    void addSphere(Sphere sphere);
    void removeSphere(Sphere sphere);
    void changeSphere(Sphere sphere, double radius) throws RadiusLogicException;
    List<Registrator> query(SphereSpecification specification);
}
