package com.epam.by.repository;

import com.epam.by.action.ComputationUtil;
import com.epam.by.entity.Registrator;
import com.epam.by.entity.Sphere;
import com.epam.by.observer.SphereObserver;

import java.util.HashMap;

public class SphereStorage {
    private static SphereStorage instance;

    private HashMap<String, Registrator> sphereObj = new HashMap<>();
    private SphereObserver observer = new SphereObserver();

    private SphereStorage() {
    }

    public static SphereStorage getInstance() {
        if (instance == null) {
            instance = new SphereStorage();
        }
        return instance;
    }


    public HashMap<String, Registrator> getSphereObj() {

        return sphereObj;
    }

    public void setSphereObj(HashMap<String, Registrator> sphereObj) {
        this.sphereObj = sphereObj;
    }

    public void addSphereToMap(Sphere sphere) {
        sphere.addObserver(observer);
        double area = ComputationUtil.computeSphereArea(sphere.getRadius());
        double volume = ComputationUtil.computeSphereVolume(sphere.getRadius());
        Registrator registrator = new Registrator(sphere, area, volume);
        sphereObj.put(sphere.getId(), registrator);

    }

    public void removeSphereFromMap(Sphere sphere) {
            sphereObj.remove(sphere.getId());
    }

    void changeSphere(Sphere sphere, double radius) {
            sphereObj.get(sphere.getId()).getSphere().changeRadius(radius);
    }

    @Override
    public String toString() {
        return "SphereStorage{" +
                "sphereObj=" + sphereObj +
                ", observer=" + observer +
                '}';
    }
}
