package com.epam.by.repository;

import com.epam.by.entity.Registrator;
import com.epam.by.entity.Sphere;
import com.epam.by.specification.SphereSpecification;

import java.util.List;

public class SphereRepository implements Repository {

    private SphereStorage storage = SphereStorage.getInstance();

    @Override
    public void addSphere(Sphere sphere) {
        storage.addSphereToMap(sphere);
    }

    @Override
    public void removeSphere(Sphere sphere) {
        storage.removeSphereFromMap(sphere);
    }

    @Override
    public void changeSphere(Sphere sphere, double radius) {
        storage.changeSphere(sphere, radius);
    }

    @Override
    public List<Registrator> query(SphereSpecification specification) {
        return specification.specified();
    }
}
