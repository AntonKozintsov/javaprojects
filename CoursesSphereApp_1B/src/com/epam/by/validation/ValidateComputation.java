package com.epam.by.validation;

import static com.epam.by.constants.ConstantsUtil.MAX_VALUE;
import static com.epam.by.constants.ConstantsUtil.SPHERE_AREA_CONST;
import static com.epam.by.constants.ConstantsUtil.VOLUME_CONST;

/**
 * com.epam.by.validation class.
 */
public final class ValidateComputation {
    /**
     * private constructor.
     */
    private ValidateComputation() {

    }
    /**
     * @param radius sphere radius
     * @return true if radius is valid for area
     */
    private static boolean validateSphereArea(final double radius) {
        return (radius > 0) && (SPHERE_AREA_CONST * radius < MAX_VALUE);

    }
    /**
     * @param radius of sphere
     * @return true if radius is valid for volume
     */
    private static boolean validateSphereVolume(final double radius) {
        return (radius > 0) && (VOLUME_CONST * Math.pow(radius, 3)) < MAX_VALUE;

    }
    /**
     * @param radius radius of sphere
     * @return true if all parameters are valid
     */
    public static boolean validate(final double radius) {
        return (validateSphereArea(radius)
                && validateSphereVolume(radius));
    }
}
