package com.epam.by.observer;

/**
 * observer pattern.
 */
public interface Observer {
    /**
     * @param event value
     */
    void handleEvent(SphereEvent event);
}
