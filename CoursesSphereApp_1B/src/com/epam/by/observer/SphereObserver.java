package com.epam.by.observer;

import com.epam.by.action.ComputationUtil;
import com.epam.by.entity.Sphere;
import com.epam.by.repository.SphereStorage;

import java.util.ArrayList;

/**
 * observer class.
 */
public class SphereObserver implements Observer {

    public ArrayList<Sphere> getList() {
        return list;
    }

    public void setList(ArrayList<Sphere> list) {
        this.list = list;
    }

    /**
     * list of spehre object.
     */
    private ArrayList<Sphere> list = new ArrayList<>();

    /**
     * @param sphere adding to list
     */
    public void addObservable(final Sphere sphere) {
        list.add(sphere);
    }

    /**
     * @param sphere removing form list
     */
    public void removeObservable(final Sphere sphere) {
        list.remove(sphere);
    }

    /**
     * @param event value
     */
    public void handleEvent(final SphereEvent event) {
        double newRadius = event.getSource().getRadius();
        double area = ComputationUtil.computeSphereArea(newRadius);
        double volume = ComputationUtil.computeSphereVolume(newRadius);
        SphereStorage sphereStorage = SphereStorage.getInstance();
        sphereStorage.getSphereObj().get(event.getSource()
                .getId()).setArea(area);
        sphereStorage.getSphereObj().get(event.getSource()
                .getId()).setVolume(volume);
    }
}
