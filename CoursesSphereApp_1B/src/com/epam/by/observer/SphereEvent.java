package com.epam.by.observer;

import com.epam.by.entity.Sphere;

import java.util.EventObject;

/**
 * class to perform event.
 */
public class SphereEvent extends EventObject {
    /**
     * @param source sphere object
     */
    public SphereEvent(final Sphere source) {
        super(source);
    }

    /**
     * @return sphere
     */
    @Override
    public Sphere getSource() {
        return (Sphere) super.getSource();
    }
}
