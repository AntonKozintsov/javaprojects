package com.epam.by.constants;

/**
 * class for com.epam.by.constants.
 */
public final class ConstantsUtil {
    /**
     * FILENAME constant for name of the file for testing.
     */
    public static final String FILE_NAME = "data/test.txt";
    /**
     * FILE_NAME_CHECK for com.epam.by.test string validations and logs.
     */
    public static final String FILE_NAME_CHECK = "data/test2.txt";
    /**
     * PATTERN is regular expression to read strings from the file.
     */
    public static final String PATTERN =
            "^\\w+  +\\s(\\d+\\.?(\\d+)?\\s){3}\\d+\\.?(\\d+)?$";
    /**
     * VOLUME_CONST to compute volume of the sphere.
     */
    public static final double VOLUME_CONST = 4.0 / 3.0 * Math.PI;
    /**
     * RATIO_CONST to compute ratio.
     */
    public static final double SPHERE_AREA_CONST = 4 * Math.PI;
    /**
     * Double max value.
     */
    public static final double MAX_VALUE = Double.MAX_VALUE;
    /**
     * private constructor.
     */
    private ConstantsUtil() {
    }
}
