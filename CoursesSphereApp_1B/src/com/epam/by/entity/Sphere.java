package com.epam.by.entity;

import com.epam.by.observer.SphereEvent;
import com.epam.by.observer.SphereObserver;

import java.util.Objects;

/**
 * sphere class.
 */
public class Sphere {
    /**
     * observer field.
     */
    private SphereObserver observer;
    /**
     * id filed.
     */
    private String id;
    /**
     * name of sphere.
     */
    private String name;
    /**
     * radius of sphere.
     */
    private double radius;
    /**
     * dot consist of 3 coordinates(x,y,z).
     */
    private Dot center;

    /**
     * @param id of sphere
     * @param name of sphere
     * @param radius of sphere
     * @param center of sphere
     */
    public Sphere(final String id, final String name, final double radius,
                  final Dot center) {
        this.id = id;
        this.name = name;
        this.radius = radius;
        this.center = center;
    }

    /**
     * @param radius to change to recompute area and volume
     */
    public void changeRadius(final double radius) {
        this.radius = radius;
        notifyObservers();
    }

    /**
     * @param observer adding to sphere
     */
    public void addObserver(final SphereObserver observer) {
        this.observer = observer;
        observer.addObservable(this);
    }

    /**
     * remove observer from object.
     */
    public void removeObserver() {
        observer.removeObservable(this);
        observer = null;
    }

    /**
     * notify observer to change values.
     */
    private void notifyObservers() {
        if (observer != null) {
            observer.handleEvent(new SphereEvent(this));
        }
    }

    /**
     * @return radius of sphere.
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @return center of sphere.
     */
    public Dot getCenter() {
        return center;
    }

    /**
     * @return id value of sphere
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return name value of sphere
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param radius value of sphere
     */
    public void setRadius(final double radius) {
        this.radius = radius;
    }
    /**
      * @return to string.
     **/
    @Override
    public String toString() {
        return "Sphere{"
                + "id=" + id
                + ", name='" + name
                + '\'' + ", radius="
                + radius + ", center="
                + center + "\n" + '}';
    }

    /**
     * @param o object param.
     * @return true if equals.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sphere)) return false;
        Sphere sphere = (Sphere) o;
        return Double.compare(sphere.radius, radius) == 0 &&
                Objects.equals(observer, sphere.observer) &&
                Objects.equals(id, sphere.id) &&
                Objects.equals(name, sphere.name) &&
                Objects.equals(center, sphere.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(observer, id, name, radius, center);
    }
}
