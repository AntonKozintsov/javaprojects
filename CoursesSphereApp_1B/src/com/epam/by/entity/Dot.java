package com.epam.by.entity;

import java.util.Objects;

/**
 *
 */
public class Dot {
    /**
     * coordinate x.
     */
    private double dotX;
    /**
     * coordinate y.
     */
    private double dotY;
    /**
     * coordinate z.
     */
    private double dotZ;

    /**
     * @param dotX coordinate X.
     * @param dotY coordinate Y.
     * @param dotZ coordinate Z.
     */
    public Dot(final double dotX, final double dotY, final double dotZ) {
        this.dotX = dotX;
        this.dotY = dotY;
        this.dotZ = dotZ;
    }

    /**
     * @return coordinate X
     */
    public double getDotX() {
        return dotX;
    }

    /**
     * @return coordinate Y
     */
    public double getDotY() {
        return dotY;
    }

    /**
     * @return coordinate Z
     */
    public double getDotZ() {
        return dotZ;
    }

    /**
     * @return to string format
     */
    @Override
    public String toString() {
        return "Dot{"
                + "dotX=" + dotX
                + ", dotY=" + dotY
                + ", dotZ=" + dotZ
                + '}';
    }

    /**
     * equals method.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dot dot = (Dot) o;
        return Double.compare(dot.getDotX(), getDotX()) == 0
                && Double.compare(dot.getDotY(), getDotY()) == 0
                && Double.compare(dot.getDotZ(), getDotZ()) == 0;
    }

    /**
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getDotX(), getDotY(), getDotZ());
    }
}
