package com.epam.by.entity;

import java.util.Objects;

public class Item {
    private Long itemId;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item fdsd = (Item) o;
        return Objects.equals(itemId, fdsd.itemId) &&
                Objects.equals(name, fdsd.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(itemId, name);
    }
}
