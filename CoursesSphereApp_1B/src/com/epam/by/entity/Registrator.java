package com.epam.by.entity;

/**
 * Class Registrator contains area , volume and sphere object.
 */
public class Registrator {
    /**
     * area of sphere.
     */
    private double area;
    /**
     * volume of sphere.
     */
    private double volume;
    /**
     * sphere object.
     */
    private Sphere sphere;
    /**
     * @param sphere sphere object
     * @param area area of sphere
     * @param volume volume of sphere
     */
    public Registrator(final Sphere sphere, final double area,
                       final double volume) {
        this.sphere = sphere;
        this.area = area;
        this.volume = volume;
    }
    /**
     * @return volume
     */
    public double getVolume() {
        return volume;
    }
    /**
     * @return area
     */
    public double getArea() {
        return area;
    }
    /**
     * @return sphere object
     */
    public Sphere getSphere() {
        return sphere;
    }
    /**
     * @param area of sphere
     */
    public void setArea(final double area) {
        this.area = area;
    }
    /**
     * @param volume of sphere
     */
    public void setVolume(final double volume) {
        this.volume = volume;
    }
    /**
     * @return formatted to string
     */
    @Override
    public String toString() {
        return "Registrator{"
                + "area=" + area
                + ", volume=" + volume
                + ", sphere=" + sphere
                + '}';
    }

}
