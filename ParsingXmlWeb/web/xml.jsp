<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>XmlParser</title>
</head>
<body>
<h1>Parser Page</h1>
<table border="2px" align="center">
    <caption>Drug catalog</caption>
    <tr>
        <th rowspan="2">Name</th>
        <th rowspan="2">Group</th>
        <th rowspan="2">Pharm</th>
        <th rowspan="2">Analog</th>
        <th colspan="6">Version</th>
    </tr>
    <tr>
        <th>Version type</th>
        <th>Organization</th>
        <th>Release date</th>
        <th>Expiration date</th>
        <th>Package Type</th>
        <th>Dosage</th>
    </tr>
    <c:forEach var="element" items="${ drugs }">
        <tr>
            <td rowspan="${ element.getVersionList().size() + 1 }"><c:out value="${ element.getName() }"/></td>
            <td rowspan="${ element.getVersionList().size() + 1 }"><c:out value="${ element.getGroup() }"/></td>
            <td rowspan="${ element.getVersionList().size() + 1 }"><c:out value="${ element.getPharm() }"/></td>
            <td rowspan="${ element.getVersionList().size() + 1 }">
                <c:forEach var="analog" items="${ element.getAnalogList() }">
                    <c:out value="${ analog }"/>
                </c:forEach>
            </td>
        </tr>
        <c:forEach var="version" items="${ element.getVersionList() }">
            <tr>
                <td><c:out value="${ version.getVersionType() }"/></td>
                <td><c:out value="${ version.getOrganization() }"/></td>
                <td><c:out value="${ version.getRelease() }"/></td>
                <td><c:out value="${ version.getExpiration()}"/></td>
                <td><c:out value="${ version.getPackageType() }"/></td>
                <td><c:out value="${ version.getDosage() }"/></td>
            </tr>
        </c:forEach>
    </c:forEach>
</table>
</body>
<a href="index.jsp">Main page</a>
</html>
