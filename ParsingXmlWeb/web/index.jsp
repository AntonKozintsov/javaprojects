<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/index.css"/>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Bot Test Servlet Page</title>
</head>
<body>
<h2>Choose File full path. Example[D:\JavaProjects\ParsingXmlWeb\data\medicins.xml]</h2>
<form method="post" action="index">
  <input type="text" name="file" />
  <fieldset class="divCl">
    <legend>Select a maintenance drone</legend>

    <div>
      <input type="radio" id="DOM" name="parser" value="DOM"/>
      <label for="DOM">DOM</label>
    </div>

    <div>
      <input type="radio" id="SAX" name="parser" value="SAX" />
      <label for="SAX">SAX</label>
    </div>

    <div>
      <input type="radio" id="StAX" name="parser" value="STAX" />
      <label for="StAX">StAX</label>
    </div>
    <input type="submit" value="Hello Servlet" />
  </fieldset>
</form>

</body>
</html>