package com.epam.by.web;

import com.epam.by.builder.BaseBuilder;
import com.epam.by.constant.Constant;
import com.epam.by.entity.Drug;
import com.epam.by.factory.BuilderFactory;
import com.epam.by.validate.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * servlet.
 */
@WebServlet(name = "Servlet", urlPatterns = {"/index"})
public class Servlet extends HttpServlet {
    /**
     * logger.
     */
    private static final Logger LOG =
            LogManager.getLogger(Servlet.class.getName());

    /**
     * @param config config.
     * @throws ServletException exception.
     */
    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {
        this.processRequest(req, resp);
        System.out.println("doget");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws ServletException, IOException {
        this.processRequest(req, resp);
        System.out.println("dopost");
    }

    /**
     * @param req  request.
     * @param resp response.
     * @throws ServletException exception.
     * @throws IOException      exception.
     */
    private void processRequest(final HttpServletRequest req,
                                final HttpServletResponse resp)
            throws ServletException, IOException {
        String fileName = req.getParameter("file");
        String parserType = req.getParameter("parser");

        ArrayList<Drug> drugList = new ArrayList<>();
        BuilderFactory builderFactory = new BuilderFactory();
        if (Validate.validate(fileName)) {

            switch (parserType) {
                case "DOM":
                    BaseBuilder domBuilder = builderFactory
                            .createBuilder(Constant.DOM_BUILDER);
                    domBuilder.buildDrugSet(fileName);
                    drugList = domBuilder.getDrugSet();
                    break;
                case "SAX":
                    BaseBuilder saxBuilder = builderFactory
                            .createBuilder(Constant.SAX_BUILDER);
                    saxBuilder.buildDrugSet(fileName);
                    drugList = saxBuilder.getDrugSet();
                    break;
                case "STAX":
                    BaseBuilder staxBuilder = builderFactory
                            .createBuilder(Constant.SAX_BUILDER);
                    staxBuilder.buildDrugSet(fileName);
                    drugList = staxBuilder.getDrugSet();
                    break;
                default:
                    break;
            }
            req.setAttribute("drugs", drugList);
            req.getRequestDispatcher("/xml.jsp").forward(req, resp);
            LOG.info("xml file successfully parased");
        } else {
            req.setAttribute("fileName", fileName);
            req.getRequestDispatcher("/errorJsp.jsp").forward(req, resp);
            LOG.error("error occurred");
        }
    }
}
