package com.epam.by.builder;

/**
 * enum.
 */
public enum DrugEnum {
    /**
     * medicins.
     */
    MEDICINS("medicins"),
    /**
     * drug.
     */
    DRUG("drug"),
    /**
     * group.
     */
    GROUP("group"),
    /**
     * name.
     */
    NAME("name"),
    /**
     * pharm.
     */
    PHARM("pharm"),
    /**
     * analogs.
     */
    ANALOGS("analogs"),
    /**
     * analog.
     */
    ANALOG("analog"),
    /**
     * version.
     */
    VERSION("version"),
    /**
     * version type.
     */
    VERSION_TYPE("versionType"),
    /**
     * organization.
     */
    ORGANIZATION("organization"),
    /**
     * release.
     */
    RELEASE("release"),
    /**
     * expiration.
     */
    EXPIRATION("expiration"),
    /**
     * package.
     */
    PACKAGE("package"),
    /**
     * package type.
     */
    PACKAGE_TYPE("packageType"),
    /**
     * dosage.
     */
    DOSAGE("dosage");
    /**
     * value.
     */
    private String value;

    /**
     * @param value string.
     */
    DrugEnum(final String value) {
        this.value = value;
    }

    /**
     * @return value.
     */
    public String getValue() {
        return value;
    }
}
