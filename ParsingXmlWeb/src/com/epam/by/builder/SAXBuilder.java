package com.epam.by.builder;

import com.epam.by.entity.Drug;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Sax builder.
 */
public class SAXBuilder extends BaseBuilder {
    /**
     * drug handler.
     */
    private DrugHandler drugHandler;
    /**
     * xml reader.
     */
    private XMLReader xmlReader;
    /**
     * list of drugs.
     */
    private ArrayList<Drug> drugs;

    /**
     * sax builder.
     */
    public SAXBuilder() {
        drugHandler = new DrugHandler();
        try {
            xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(drugHandler);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }


    /**
     * @param filename xml filename.
     */
    @Override

    public void buildDrugSet(final String filename) {
        try {
            xmlReader.parse(filename);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        drugs = drugHandler.getDrugs();

    }

    /**
     * @return list of drugs.
     */
    @Override
    public ArrayList<Drug> getDrugSet() {
        return drugs;
    }
}
