package com.epam.by.builder;

import com.epam.by.entity.Drug;
import com.epam.by.entity.Version;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * stax builder.
 */
public class StAXBuilder extends BaseBuilder {
    /**
     * list of drugs.
     */
    private ArrayList<Drug> drugs = new ArrayList<>();
    /**
     * xml input factory.
     */
    private XMLInputFactory xmlInputFactory;

    /**
     * stax builder.
     */
    public StAXBuilder() {
        xmlInputFactory = XMLInputFactory.newInstance();
    }

    /**
     * @return list of drugs.
     */
    @Override
    public ArrayList<Drug> getDrugSet() {
        return drugs;
    }

    /**
     * @param filename xml filename.
     */
    @Override
    public void buildDrugSet(final String filename) {
        FileInputStream inputStream = null;
        XMLStreamReader xmlStreamReader = null;
        String name;
        try {
            inputStream = new FileInputStream(new File(filename));
            xmlStreamReader = xmlInputFactory
                    .createXMLStreamReader(inputStream);
            while (xmlStreamReader.hasNext()) {
                int type = xmlStreamReader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = xmlStreamReader.getLocalName();
                    if (DrugEnum.valueOf(name.toUpperCase()) == DrugEnum.DRUG) {
                        Drug drug = buildDrug(xmlStreamReader);
                        drugs.add(drug);
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param reader reader.
     * @return drug entity.
     * @throws XMLStreamException exception.
     */
    private Drug buildDrug(final XMLStreamReader reader)
            throws XMLStreamException {
        String name;
        ArrayList<Version> versionArrayList = new ArrayList<>();
        Drug drug = new Drug();
        drug.setGroup(reader.getAttributeValue(0));
        while (reader.hasNext()) {
            int type = reader.next();

            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (DrugEnum.valueOf(name.toUpperCase())
                            == DrugEnum.VERSION) {
                        String string = reader.getAttributeValue(null,
                                DrugEnum.VERSION_TYPE.getValue());
                        Version version = buildVersion(reader, string);
                        versionArrayList.add(version);
                        drug.setVersionList(versionArrayList);
                    }
                    switch (DrugEnum.valueOf(name.toUpperCase())) {
                        case NAME:
                            drug.setName(getXMLText(reader));
                            break;
                        case PHARM:
                            drug.setPharm(getXMLText(reader));
                            break;
                        case ANALOGS:
                            drug.setAnalogList(buildAnalogs(reader));
                        default:
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DrugEnum.valueOf(name.toUpperCase()) == DrugEnum.DRUG) {

                        return drug;
                    }
                    break;
                default:
                    break;
            }

        }
        throw new XMLStreamException();
    }

    /**
     * @param reader reader.
     * @return analog list.
     * @throws XMLStreamException exception.
     */
    private ArrayList<String> buildAnalogs(final XMLStreamReader reader)
            throws XMLStreamException {
        String name;
        ArrayList<String> analogList = new ArrayList<>();
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DrugEnum.valueOf(name.toUpperCase())) {
                        case ANALOG:
                            analogList.add(getXMLText(reader));
                            break;
                        default:
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DrugEnum.valueOf(name.toUpperCase())
                            == DrugEnum.ANALOGS) {
                        return analogList;
                    }
                    break;
                default:
                    break;
            }

        }
        throw new XMLStreamException("Unknown element in tag StudentANALOG");
    }

    /**
     * @param reader reader.
     * @param string string.
     * @return version.
     * @throws XMLStreamException exception.
     */
    private Version buildVersion(final XMLStreamReader reader,
                                 final String string)
            throws XMLStreamException {
        Version version = new Version();
        version.setVersionType(string);
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DrugEnum.valueOf(name.toUpperCase())) {

                        case ORGANIZATION:
                            version.setOrganization(reader
                                    .getAttributeValue(null,
                                            DrugEnum.ORGANIZATION.getValue()));
                            break;
                        case RELEASE:
                            version.setRelease(getXMLText(reader));
                            break;
                        case EXPIRATION:
                            version.setExpiration(getXMLText(reader));
                            break;
                        case PACKAGE:
                            version.setPackageType(reader
                                    .getAttributeValue(null,
                                            DrugEnum.PACKAGE_TYPE.getValue()));
                            break;
                        case DOSAGE:
                            version.setDosage(getXMLText(reader));
                            break;
                        default:
                            break;
                    }

                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DrugEnum.valueOf(name.toUpperCase())
                            == DrugEnum.VERSION) {
                        return version;
                    }
                    break;
                default:
                    break;
            }

        }
        throw new XMLStreamException("Unknown element in tag StudentVERSION");
    }

    /**
     * @param reader reader.
     * @return string.
     * @throws XMLStreamException exception.
     */
    private String getXMLText(final XMLStreamReader reader)
            throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
