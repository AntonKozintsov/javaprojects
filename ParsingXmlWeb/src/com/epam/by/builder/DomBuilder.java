package com.epam.by.builder;

import com.epam.by.entity.Drug;
import com.epam.by.entity.Version;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Dom builder.
 */
public class DomBuilder extends BaseBuilder {
    /**
     * document builder.
     */
    private DocumentBuilder documentBuilder;
    /**
     * lis of drugs.
     */
    private ArrayList<Drug> drugs;

    /**
     * @return list of drugs.
     */
    @Override
    public ArrayList<Drug> getDrugSet() {
        return drugs;
    }

    /**
     * dom builder.
     */
    public DomBuilder() {
        this.drugs = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param drugElement drug element.
     * @return drug entity.
     */
    private Drug buildDrug(final Element drugElement) {

        Drug drug = new Drug();
        drug.setGroup(drugElement.getAttribute("group"));
        drug.setName(getElementTextContent(drugElement, "name"));
        drug.setPharm(getElementTextContent(drugElement, "pharm"));
        drug.setAnalogList(buildAnalogs(drugElement));
        drug.setVersionList(buildVersion(drugElement));
        return drug;
    }

    /**
     * @param drugSet list of drugs.
     */
    public DomBuilder(final ArrayList<Drug> drugSet) {
        super(drugSet);
    }

    /**
     * @param fileName xml.
     */
    @Override
    public void buildDrugSet(final String fileName) {
        Document document;
        try {
            document = documentBuilder.parse(fileName);
            Element root = document.getDocumentElement();
            NodeList drugList = root.getElementsByTagName("drug");
            for (int i = 0; i < drugList.getLength(); i++) {
                Element drugElement = (Element) drugList.item(i);
                Drug drug = buildDrug(drugElement);
                drugs.add(drug);

            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * @param drugElement drug.
     * @return list of version.
     */
    private ArrayList<Version> buildVersion(final Element drugElement) {
        ArrayList<Version> versionArrayList = new ArrayList<>();
        NodeList versionList = drugElement.getElementsByTagName("version");
        for (int i = 0; i < versionList.getLength(); i++) {
            Version version = new Version();
            Element versionElement = (Element) drugElement
                    .getElementsByTagName("version").item(i);
            version.setVersionType(versionElement
                    .getAttribute("versionType"));
            Element organizationElement = (Element) drugElement.
                    getElementsByTagName("organization").item(i);
            version.setOrganization(organizationElement
                    .getAttribute("organization"));
            version.setRelease(getElementTextContent(versionElement,
                    "release"));
            version.setExpiration(getElementTextContent(versionElement,
                    "expiration"));
            Element packageType = (Element) drugElement
                    .getElementsByTagName("package").item(i);
            version.setPackageType(packageType
                    .getAttribute("packageType"));
            version.setDosage(getElementTextContent(versionElement,
                    "dosage"));
            versionArrayList.add(version);
        }
        return versionArrayList;
    }

    /**
     * @param drugElement drug.
     * @return list of analogs.
     */
    private static ArrayList<String> buildAnalogs(final Element drugElement) {
        ArrayList<String> analogArrayList = new ArrayList<>();
        NodeList analogList = drugElement.getElementsByTagName("analog");
        for (int i = 0; i < analogList.getLength(); i++) {
            analogArrayList.add(analogList.item(i).getTextContent());
        }
        return analogArrayList;
    }

    /**
     * @param element     element
     * @param elementName elementName
     * @return string.
     */
    private static String getElementTextContent(final Element element,
                                                final String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }
}
