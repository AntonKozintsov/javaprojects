package com.epam.by.builder;

import com.epam.by.entity.Drug;
import com.epam.by.entity.Version;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.EnumSet;

/**
 * drug handler.
 */
public class DrugHandler extends DefaultHandler {
    /**
     * list of drugs.
     */
    private ArrayList<Drug> drugs;
    /**
     * current drug.
     */
    private Drug current = null;
    /**
     * current enum.
     */
    private DrugEnum currentEnum = null;
    /**
     * current enum.
     */
    private EnumSet<DrugEnum> withText;
    /**
     * current analogs.
     */
    private ArrayList<String> currentAnalogs;
    /**
     * current versions.
     */
    private ArrayList<Version> currentVersions;
    /**
     * current version.
     */
    private Version currentVersion;

    /**
     * drug handler.
     */
    public DrugHandler() {
        drugs = new ArrayList<>();
        withText = EnumSet.range(DrugEnum.NAME, DrugEnum.DOSAGE);
    }

    /**
     * @return list of drugs.
     */
    public ArrayList<Drug> getDrugs() {
        return drugs;
    }

    /**
     * @param uri        uri.
     * @param localName  localName.
     * @param qName      qName.
     * @param attributes attributes.
     * @throws SAXException exception.
     */
    @Override
    public void startElement(final String uri, final String localName,
                             final String qName,
                             final Attributes attributes) throws SAXException {
        switch (localName) {
            case "drug":
                current = new Drug();
                current.setGroup(attributes.getValue(0));
                currentVersions = new ArrayList<>();
                break;
            case "analogs":
                currentAnalogs = new ArrayList<String>();
                break;

            case "version":
                currentVersion = new Version();
                currentVersion.setVersionType(attributes.getValue(0));
                break;

            case "organization":
                currentVersion.setOrganization(attributes.getValue(0));

            case "package":
                currentVersion.setPackageType(attributes.getValue(0));

            default:
                DrugEnum temp = DrugEnum.valueOf(localName.toUpperCase());
                if (withText.contains(temp)) {
                    currentEnum = temp;
                }
        }
    }

    /**
     * @param uri       uri.
     * @param localName localName.
     * @param qName     qName.
     * @throws SAXException exception.
     */
    @Override
    public void endElement(final String uri, final String localName,
                           final String qName) throws SAXException {
        switch (localName) {
            case "drug":
                drugs.add(current);
                break;

            case "analogs":
                current.setAnalogList(currentAnalogs);

            case "version":
                current.setVersionList(currentVersions);

        }
    }

    /**
     * @param ch     char.
     * @param start  start.
     * @param length length.
     * @throws SAXException exception.
     */
    @Override
    public void characters(final char[] ch, final int start,
                           final int length) throws SAXException {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    current.setName(s);
                    break;
                case PHARM:
                    current.setPharm(s);
                    break;
                case ANALOG:
                    currentAnalogs.add(s);
                    break;
                case VERSION_TYPE:
                    currentVersion.setVersionType(s);
                    break;
                case RELEASE:
                    currentVersion.setRelease(s);
                    break;
                case EXPIRATION:
                    currentVersion.setExpiration(s);
                    break;
                case DOSAGE:
                    currentVersion.setDosage(s);
                    currentVersions.add(currentVersion);
                    break;
                case ORGANIZATION:
                    break;
                case PACKAGE:
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.
                            getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}
