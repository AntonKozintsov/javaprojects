package com.epam.by.builder;

import com.epam.by.entity.Drug;

import java.util.ArrayList;

/**
 * Builder abstract class.
 */
public abstract class BaseBuilder {
    /**
     * list of drugs.
     */
    private ArrayList<Drug> drugSet;

    /**
     * Base builder.
     */
    public BaseBuilder() {
        drugSet = new ArrayList<>();
    }

    /**
     * @param drugSet list of drugs.
     */
    public BaseBuilder(final ArrayList<Drug> drugSet) {
        this.drugSet = drugSet;
    }

    /**
     * @return list of drugs.
     */
    public ArrayList<Drug> getDrugSet() {
        return drugSet;
    }

    /**
     * @param filename xml filename
     */
    public abstract void buildDrugSet(String filename);
}
