package com.epam.by.constant;

import javax.xml.XMLConstants;

/**
 * Class for constant.
 */
public final class Constant {
    /**
     * private constructor.
     */
    private Constant() {
    }
    /**
     * Dom.
     */
    public static final String DOM_BUILDER = "DOM";
    /**
     * Sax.
     */
    public static final String SAX_BUILDER = "SAX";
    /**
     * Stax.
     */
    public static final String STAX_BUILDER = "STAX";
    /**
     * schema path.
     */
    public static final String SCHEMA_NAME =
            "D:\\JavaProjects\\ParsingXmlWeb\\data\\medicinsXSD.xsd";
    /**
     * language schema.
     */
    public static final String LANGUAGE = XMLConstants.W3C_XML_SCHEMA_NS_URI;
}
