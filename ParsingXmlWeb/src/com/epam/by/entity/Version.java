package com.epam.by.entity;

/**
 * Version.
 */
public class Version {
    /**
     * version type.
     */
    private String versionType;
    /**
     * organization.
     */
    private String organization;
    /**
     * release.
     */
    private String release;
    /**
     * expiration.
     */
    private String expiration;
    /**
     * package type.
     */
    private String packageType;
    /**
     * dosage.
     */
    private String dosage;
    /**
     * {@inheritDoc}
     */
    public void setVersionType(final String versionType) {
        this.versionType = versionType;
    }
    /**
     * {@inheritDoc}
     */
    public void setOrganization(final String organization) {
        this.organization = organization;
    }
    /**
     * {@inheritDoc}
     */
    public void setRelease(final String release) {
        this.release = release;
    }

    /**
     * {@inheritDoc}
     */
    public void setExpiration(final String expiration) {
        this.expiration = expiration;
    }

    /**
     * {@inheritDoc}
     */
    public void setPackageType(final String packageType) {
        this.packageType = packageType;
    }
    /**
     * {@inheritDoc}
     */
    public void setDosage(final String dosage) {
        this.dosage = dosage;
    }
    /**
     * {@inheritDoc}
     */
    public String getVersionType() {
        return versionType;
    }
    /**
     * {@inheritDoc}
     */
    public String getOrganization() {
        return organization;
    }
    /**
     * {@inheritDoc}
     */
    public String getRelease() {
        return release;
    }
    /**
     * {@inheritDoc}
     */
    public String getExpiration() {
        return expiration;
    }
    /**
     * {@inheritDoc}
     */
    public String getPackageType() {
        return packageType;
    }
    /**
     * {@inheritDoc}
     */
    public String getDosage() {
        return dosage;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Version{" + "versionType='" + versionType
                + '\'' + ", organization='" + organization
                + '\'' + ", release='" + release + '\'' + ", expiration='"
                + expiration + '\'' + ", packageType='" + packageType
                + '\'' + ", dosage='" + dosage + '\'' + '}' + "\n";
    }
}
