package com.epam.by.entity;

import java.util.ArrayList;

/**
 * Drug entity.
 */
public class Drug {
    /**
     * group.
     */
    private String group;
    /**
     * name.
     */
    private String name;
    /**
     * pharm.
     */
    private String pharm;
    /**
     * analog list.
     */
    private ArrayList<String> analogList;
    /**
     * version list.
     */
    private ArrayList<Version> versionList;

    /**
     * @return group.
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group string.
     */
    public void setGroup(final String group) {
        this.group = group;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    public String getPharm() {
        return pharm;
    }

    /**
     * {@inheritDoc}
     */
    public void setPharm(final String pharm) {
        this.pharm = pharm;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<Version> getVersionList() {
        return versionList;
    }

    /**
     * {@inheritDoc}
     */
    public void setVersionList(final ArrayList<Version> versionList) {
        this.versionList = versionList;
    }

    /**
     * {@inheritDoc}
     */
    public ArrayList<String> getAnalogList() {
        return analogList;
    }

    /**
     * {@inheritDoc}
     */
    public void setAnalogList(final ArrayList<String> analogList) {
        this.analogList = analogList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Drug{" + "group='" + group + '\''
                + ", name='" + name + '\'' + ", pharm='"
                + pharm + '\'' + ", analogList="
                + analogList + ", versionList=" + versionList
                + '}' + "\n";
    }
}
