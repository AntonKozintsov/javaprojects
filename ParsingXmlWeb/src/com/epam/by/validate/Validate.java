package com.epam.by.validate;

import com.epam.by.constant.Constant;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * validate xml file.
 */
public final class Validate {
    /**
     * private constructor.
     */
    private Validate() {
    }

    /**
     * @param fileName xml filename.
     * @return boolean.
     */
    public static boolean validate(final String fileName) {
        SchemaFactory factory = SchemaFactory.newInstance(Constant.LANGUAGE);
        File schemaLocation = new File(Constant.SCHEMA_NAME);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);
            return true;
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }

    }
}
