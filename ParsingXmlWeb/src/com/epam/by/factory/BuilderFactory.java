package com.epam.by.factory;

import com.epam.by.builder.BaseBuilder;
import com.epam.by.builder.DomBuilder;
import com.epam.by.builder.SAXBuilder;
import com.epam.by.builder.StAXBuilder;

/**
 * Builder factory.
 */
public class BuilderFactory {
    /**
     * enum.
     */
    private enum ParserType {
        /**
         * sax.
         */
        SAX,
        /**
         * stax.
         */
        STAX,
        /**
         * dom.
         */
        DOM;
    }

    /**
     * @param parserType parser type.
     * @return builder.
     */
    public BaseBuilder createBuilder(final String parserType) {

        ParserType type = ParserType.valueOf(parserType.toUpperCase());

        switch (type) {
            case DOM:
                return new DomBuilder();
            case SAX:
                return new SAXBuilder();
            case STAX:
                return new StAXBuilder();
            default:
                throw new EnumConstantNotPresentException(type
                        .getDeclaringClass(), type.name());
        }

    }
}
