//package com.epam.by.test;
//
//import com.epam.by.validate.Validate;
//import org.testng.Assert;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//public class ValidateTest {
//    @DataProvider(name = "validateXML")
//    public static Object[][] validateXML() {
//        String validFile = "data/medicins.xml";
//        String brokenFile = "data/wrongXML.xml";
//        return new Object[][]{{validFile,true}, {brokenFile,false}};
//    }
//    @Test(dataProvider = "validateXML")
//    public void testValidate(String fileName, boolean expectedResult) {
//        Assert.assertEquals(Validate.validate(fileName),expectedResult);
//    }
//}