package com.epam.by.read;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static com.epam.by.constant.Constant.FILE_NAME;

/**
 * Class to read information from file.
 */
public final class ReadFile {
    /**
     * private constructor.
     */
    private ReadFile() {
    }
    /**
     * Logger.
     */
    private static final Logger LOG_ERROR = LogManager.getLogger("ERROR");

    /**
     * @return String
     * @throws IOException exception
     */
    public static String readFile() throws IOException {
        String resultString;
        try {
            resultString = Files.lines(Paths.get(FILE_NAME))
                    .collect(Collectors.joining());
            return resultString;
        } catch (IOException e) {
            LOG_ERROR.log(Level.ERROR, "Error while load information form file");
            throw new IOException("File error");
        }
    }
}
