package com.epam.by.composite;

/**
 * Component interface.
 */
public interface Component {
    /**
     * execute operation.
     */
    String operation();

    /**
     * method to add component.
     * @param component component
     */
    void add(Component component);

    /**
     * method to remove component.
     * @param component component
     */
    void remove(Component component);

    /**
     * method to get child.
     * @param index ndex
     * @return child
     */
    Object getChild(int index);
}
