package com.epam.by.composite;

import com.epam.by.constant.TextType;
import com.epam.by.specification.parse.ParseGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.epam.by.constant.TextType.LEXEME;
import static com.epam.by.constant.TextType.PARAGRAPH;

/**
 * Composite class.
 */
public class Composite implements Component {
    /**
     * type.
     */
    private TextType type;
    /**
     * List of components.
     */
    private List<Component> components;

    /**
     * @param type   type
     * @param string string
     */
    public Composite(final TextType type, final String string) {
        this.type = type;
        this.components = new ArrayList<>();
        for (Component component : ParseGroup.parse(type, string)) {
            this.add(component);
        }
    }

    /**
     * method to print text.
     */
    @Override
    public String operation() {

        final StringBuilder builder = new StringBuilder();
        final String resultString;

        if (this.type == LEXEME) {
            builder.append(" ");
        }

        for (final Component component : this.components) {
            builder.append(component.operation());
        }

        if (this.type == PARAGRAPH) {
            builder.append("\n");
        }

        resultString = builder.toString();

        return resultString;

    }

    /**
     * method to add component.
     *
     * @param component component
     */
    @Override
    public void add(final Component component) {
        this.components.add(component);
    }

    /**
     * method to remove component.
     * @param component component
     */
    @Override
    public void remove(final Component component) {
        this.components.remove(component);
    }

    /**
     * @param index ndex
     * @return component
     */
    @Override
    public Object getChild(final int index) {
        return components.get(index);
    }

    /**
     * @return type
     */
    public TextType getType() {
        return type;
    }

    /**
     * @param type type
     */
    public void setType(final TextType type) {
        this.type = type;
    }

    /**
     * @return list of components
     */
    public List<Component> getComponents() {
        return components;
    }

    /**
     * @param components components
     */
    public void setComponents(final List<Component> components) {
        this.components = components;
    }

    /**
     * @param o oject
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Composite composite = (Composite) o;
        return type == composite.type
                & Objects.equals(components, composite.components);
    }

    /**
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(type, components);
    }

    /**
     * method to string.
     * @return to string
     */
    @Override
    public String toString() {
        return "Composite{"
                + "type=" + type
                + ", components="
                + components
                + '}';
    }
}
