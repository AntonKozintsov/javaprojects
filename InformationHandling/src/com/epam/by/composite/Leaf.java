package com.epam.by.composite;

import java.util.Objects;

/**
 * Leaf class.
 */
public class Leaf implements Component {
    /**
     * symbol.
     */
    private char symbol;

    /**
     * @param symbol symbol
     */
    public Leaf(final char symbol) {
        this.symbol = symbol;
    }

    /**
     * @return String
     */
    @Override
    public String operation() {
        return Character.toString(this.symbol);
    }

    @Override
    public void add(final Component component) {
        //this is leaf node so this method is not applicable to this class.
    }

    @Override
    public void remove(final Component component) {
        //this is leaf node so this method is not applicable to this class.
    }

    /**
     * @param index index
     * @return null
     */
    @Override
    public Object getChild(final int index) {
        //this is leaf node so this method is not applicable to this class.
        return null;
    }

    /**
     * @return symbol
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * @param symbol symbol
     */
    public void setSymbol(final char symbol) {
        this.symbol = symbol;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Leaf leaf = (Leaf) o;
        return symbol == leaf.symbol;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(symbol);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Leaf{" + "symbol=" + symbol
                + '}';
    }
}
