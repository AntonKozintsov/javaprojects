package com.epam.by.constant;

/**
 * Text types.
 */
public enum TextType {
    /**
     * Text.
     */
    TEXT,
    /**
     * Paragraph.
     */
    PARAGRAPH,
    /**
     * Sentence.
     */
    SENTENCE,
    /**
     * Expression.
     */
    EXPRESSION,
    /**
     * Lexeme.
     */
    LEXEME,
    /**
     * Word.
     */
    WORD,
}
