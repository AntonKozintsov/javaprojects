package com.epam.by.constant;

/**
 * Class for constants.
 */
public final class Constant {
    /**
     * private constructor.
     */
    private Constant() {
    }

    /**
     * Constant to parse Paragraph.
     */
    public static final String PARAGRAPH = "\\t";
    /**
     * Constant to parse Sentence.
     */
    public static final String SENTENCE =
            "[A-Z][a-zA-Z\\d~&|^\\(\\)><,\\s'-]+[.!?]";
    /**
     * Constant to parse Lexeme.
     */
    public static final String LEXEME = "\\s";
    /**
     * Constant to parse Word.
     */
    public static final String WORD = "[A-Za-z'-]+";
    /**
     * Constant to parse Expression.
     */
    public static final String EXPRESSION = "[\\d~&|^\\(\\)><]{2,}";
    /**
     * File name to read information from.
     */
    public static final String FILE_NAME = "data/text.txt";
}
