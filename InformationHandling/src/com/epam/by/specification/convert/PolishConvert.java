package com.epam.by.specification.convert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for polish convert.
 */
public final class PolishConvert {
    /**
     * private constructor.
     */
    private PolishConvert() {
    }

    /**
     * Priority.
     */
    private static HashMap<String, Integer> operationPriority;
    static {
        operationPriority = new HashMap<>();
        operationPriority.put("|", 12);
        operationPriority.put("&", 10);
        operationPriority.put("~", 3);
        operationPriority.put("^", 11);
        operationPriority.put(">>", 7);
        operationPriority.put("<<", 7);
        operationPriority.put(">>>", 7);
        operationPriority.put("<<<", 7);
    }

    /**
     * @param expression expression.
     * @return String.
     */
    public static String convertToPolishNotation(String expression) {
        String result = "";
        Stack<String> stack = new Stack<String>();
        Pattern pattern = Pattern.compile(
                "^([0-9]+|\\||\\&|\\(|\\)|\\^|~|<<<|>>>|<<|>>)");
        Matcher matcher = pattern.matcher(expression);
        while (matcher.find()) {
            String part = matcher.group();
            if (part.matches("[0-9]+")) {
                result = String.join(" ", result, part);
            } else {
                if (part.equals("~") || part.equals("(")) {
                    stack.push(part);
                } else {
                    if (part.equals(")")) {
                        while (true) {
                            part = stack.pop();
                            if (part.equals("(")) {
                                break;
                            }
                            result = String.join(" ", result, part);
                        }
                    } else {
                        String operation;
                        while (!stack.isEmpty()) {
                            operation = stack.pop();
                            if (operation.equals("(")
                                    || operationPriority.get(operation)
                                    > operationPriority.get(part)) {
                                stack.push(operation);
                                break;
                            }
                            result = String.join(" ", result, operation);
                        }
                        stack.push(part);
                    }
                }
            }
            expression = expression.substring(part.length());
            matcher = pattern.matcher(expression);
        }
        while (!stack.isEmpty()) {
            result = String.join(" ", result, stack.pop());
        }
        return result.trim();
    }

    /**
     * @param expression exoression
     * @return calculated polish converted string
     */
    public static String calculate(String expression) {
        ArrayList<String> symbols =
                new ArrayList<String>(Arrays.asList(expression.split(" ")));
        Stack<String> stack = new Stack<String>();
        for (String symbol : symbols) {
            if (symbol.matches("[0-9]+")) {
                stack.push(symbol);
            } else {
                if (symbol.equals("~")) {
                    stack.push(String.valueOf(~Integer.parseInt(stack.pop())));
                    continue;
                }
                int operand1 = Integer.parseInt(stack.pop());
                int operand2 = Integer.parseInt(stack.pop());
                switch (symbol) {
                    case "&":
                        stack.push(String.valueOf(operand1 & operand2));
                        break;
                    case "|":
                        stack.push(String.valueOf(operand1 | operand2));
                        break;
                    case "^":
                        stack.push(String.valueOf(operand1 ^ operand2));
                        break;
                    case ">>":
                        stack.push(String.valueOf(operand2 >> operand1));
                        break;
                    case "<<":
                        stack.push(String.valueOf(operand2 << operand1));
                        break;
                    case ">>>":
                        stack.push(String.valueOf(operand2 >>> operand1));
                        break;
                    case "<<<":
                        stack.push(String.valueOf(operand2 << operand1));
                        break;
                    default:
                        //exception
                        break;
                }
            }
        }
        return stack.pop();
    }
}
