package com.epam.by.specification.sort;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.composite.Leaf;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.by.constant.TextType.TEXT;

/**
 * Class to sort lexeme.
 */
public final class SortLexeme {
    /**
     * private constructor.
     */
    private SortLexeme() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(SortParagraph.class.getName());
    /**
     * @param component component
     * @param symbol symbol
     * @return return List of components.
     */
    public static List<Component> sortingLexemes(
            final Component component, final char symbol) {

        final Composite text = (Composite) component;
        List<Component> components = new ArrayList<>();
        Composite composite;

        if (text.getType() == TEXT) {

            for (final Component comp1 : text.getComponents()) {

                composite = (Composite) comp1;

                for (final Component comp2 : composite.getComponents()) {
                    components.addAll(((Composite) comp2).getComponents());
                }

            }

            components = components.stream()
                    .sorted((component1, component2) -> {

                        final String string1 = component1.operation();
                        final String string2 = component2.operation();

                        final int value;
                        final int count1 = countCharacters(component1, symbol);
                        final int count2 = countCharacters(component2, symbol);

                        if (count1 == 0 && count2 == 0) {
                            value = string1.compareTo(string2);
                        } else {
                            value = Integer.compare(count2, count1);
                        }

                        return value;

                    }).collect(Collectors.toList());

            LOG_INFO.log(Level.INFO, "Lexeme sorted successfully");
        }
        return components;
    }

    /**
     * This method calculates number of occurrences of character in lexeme.
     *
     * @param component value of the object Component
     * @param sym value of the character
     * @return count of symbols
     * */
    private static int countCharacters(final Object component,
                                       final char sym) {

        final Composite lexeme = (Composite) component;
        int count = 0;

        for (final Component data : lexeme.getComponents()) {

            if (data instanceof Leaf) {
                if (((Leaf) data).getSymbol() == sym) {
                    count++;
                }
            } else {
                for (final Component comp
                        :((Composite) data).getComponents()) {

                    if (comp instanceof Leaf) {
                        if (((Leaf) comp).getSymbol() == sym) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

}
