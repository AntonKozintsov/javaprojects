package com.epam.by.specification.sort;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to sort paragraph.
 */
public final class SortParagraph {
    /**
     * private constructor.
     */
    private SortParagraph() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(SortParagraph.class.getName());

    /**
     * @param text text
     * @return Composite
     */
    public static Composite sortParagraph(final Composite text) {

        final Composite composite = new Composite(TextType.TEXT, "");
        final List<Component> paragraph = text.getComponents();

        composite.getComponents().addAll(
                paragraph.stream().sorted(Comparator.comparing(
                (Component comp) -> ((Composite) comp).getComponents().size()))
                .collect(Collectors.toList()));
        LOG_INFO.log(Level.INFO, "Paragraph sorted successfully");
        return composite;
    }
}
