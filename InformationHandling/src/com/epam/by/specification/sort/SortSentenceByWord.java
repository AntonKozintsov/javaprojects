package com.epam.by.specification.sort;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.composite.Leaf;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.epam.by.constant.TextType.WORD;

/**
 * Class to sort sentence.
 */
public final class SortSentenceByWord {
    /**
     * private constructor.
     */
    private SortSentenceByWord() {
    }

    /**
     * To log.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(SortSentenceByWord.class.getName());

    /**
     * @param sentence sentence
     * @return Component
     */
    public static Component sortSentenceByWord(final Composite sentence) {

        final List<Component> words = new ArrayList<>();
        for (final Component comp1 : sentence.getComponents()) {
            for (final Component comp2 : ((Composite) comp1).getComponents()) {

                if (comp2 instanceof Leaf) {
                    continue;
                }
                if (((Composite) comp2).getType() == WORD) {
                    words.add(comp2);
                }
            }
        }
        words.sort(Comparator.comparing((Component comp) -> ((Composite) comp)
                .getComponents().size()));
        int index = 0;
        for (final Component comp1 : sentence.getComponents()) {

            if (!(((Composite) comp1).getComponents().get(0) instanceof Leaf)
                    && ((Composite) ((Composite) comp1)
                    .getComponents().get(0)).getType() == WORD) {
                ((Composite) comp1).getComponents().set(0, words.get(index));
                index++;
            }
        }
        LOG_INFO.log(Level.INFO, "Sentence sorted successfully");
        return sentence;
    }

}
