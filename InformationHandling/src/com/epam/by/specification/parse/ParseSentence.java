package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse Sentence.
 */
public final class ParseSentence {
    /**
     * private constructor.
     */
    private ParseSentence() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseSentence.class.getName());

    /**
     * @param string string
     * @return List of components
     */
    public static List<Component> parse(final String string) {
        Pattern pattern = Pattern.compile(Constant.SENTENCE);
        Matcher matcher = pattern.matcher(string);

        List<Component> components = new ArrayList<>();
        while (matcher.find()) {
            String sentence = matcher.group();
            components.add(new Composite(TextType.SENTENCE, sentence));
        }
        LOG_INFO.log(Level.INFO, "Parse SENTENCE completed successfully");
        return components;
    }
}
