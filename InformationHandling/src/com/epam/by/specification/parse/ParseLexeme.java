package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse Lexeme.
 */
public final class ParseLexeme {
    /**
     * private constructor.
     */
    private ParseLexeme() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseLexeme.class.getName());

    /**
     * @param string string
     * @return List of components
     */
    public static List<Component> parse(final String string) {


        List<Component> components = new ArrayList<>();
        String [] element  = string
                .trim()
                .split(Constant.LEXEME);

        for (String lexeme : element) {
            components.add(new Composite(TextType.LEXEME, lexeme));
        }
        LOG_INFO.log(Level.INFO, "Parse LEXEME completed successfully");
        return components;
    }
}
