package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Leaf;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse text.
 */
public final class ParseGroup {
    /**
     * private constructor.
     */
    private ParseGroup() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_ERROR = LogManager.getLogger("ERROR");

    /**
     * @param type type
     * @param string String
     * @return List of components.
     */
    public static List<Component> parse(final TextType type,
                                        final String string) {
        List<Component> components = new ArrayList<>();

        switch (type) {
            case TEXT:
                components.addAll(ParseParagraph.parse(string));
                break;

            case PARAGRAPH:
                components.addAll(ParseSentence.parse(string));
                break;
            case SENTENCE:
                components.addAll(ParseLexeme.parse(string));
                break;
            case LEXEME:
                if (ParseExpression.checkTypeOfString(string,
                        Constant.EXPRESSION)) {
                    components.addAll(
                            ParseExpression.parse(string));
                } else {
                    char symbol = string.charAt(0);
                    if (symbol == '(') {
                        components.add(new Leaf(symbol));
                    }
                    components.addAll(
                            ParseWord.parse(string));
                    String[] symbols = string.split(
                            Constant.WORD);
                    if (symbols.length > 1) {
                        for (char c : symbols[symbols.length - 1]
                                .toCharArray()) {
                            components.add(new Leaf(c));
                        }
                    }
                }
                break;
            case WORD:
                components.addAll(ParseSymbol.parse(string));

                break;
            case EXPRESSION:
                components.addAll(ParseSymbol.parse(string));
                break;
            default:
                LOG_ERROR.log(Level.ERROR, "Parse GROUP interrupted");
        }
        return components;
    }
}
