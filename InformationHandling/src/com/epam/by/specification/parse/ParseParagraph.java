package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse paragraph.
 */
public final class ParseParagraph {
    /**
     * private constructor.
     */
    private ParseParagraph() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseParagraph.class.getName());

    /**
     * @param string string
     * @return list of components
     */
    public static List<Component> parse(final String string) {

        String[] element = string
                .trim()
                .split(Constant.PARAGRAPH);

        List<Component> components = new ArrayList<>();
        for (String paragraph : element) {
            components.add(new Composite(TextType.PARAGRAPH, paragraph));

        }
        LOG_INFO.log(Level.INFO, "Parse PARAGRAPH completed successfully");
        return components;
    }

}
