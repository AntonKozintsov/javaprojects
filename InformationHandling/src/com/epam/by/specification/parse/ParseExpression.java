package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import com.epam.by.specification.convert.PolishConvert;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse Expression.
 */
public final class ParseExpression {
    /**
     * private constructor.
     */
    private ParseExpression() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseExpression.class.getName());

    /**
     * @param string string
     * @return List of components
     */
    public static List<Component> parse(final String string) {
        Pattern pattern = Pattern.compile(Constant.EXPRESSION);
        Matcher matcher = pattern.matcher(string);

        List<Component> components = new ArrayList<>();
        while (matcher.find()) {
            String expression = matcher.group();
            components.add(new Composite(TextType.EXPRESSION, PolishConvert
                    .calculate(PolishConvert
                            .convertToPolishNotation(expression))));
        }
        LOG_INFO.log(Level.INFO, "Parse EXPRESSION completed successfully");
        return components;
    }

    /**
     * @param string string
     * @param regex regex
     * @return boolean
     */
    public static boolean checkTypeOfString(final String string,
                                            final String regex) {

        final boolean flag;
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(string);

        flag = matcher.find();

        return flag;

    }
}
