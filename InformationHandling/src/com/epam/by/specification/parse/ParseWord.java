package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.Constant;
import com.epam.by.constant.TextType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse word.
 */
public final class ParseWord {
    /**
     * private constructor.
     */
    private ParseWord() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseWord.class.getName());

    /**
     * @param string string
     * @return List of components.
     */
    public static List<Component> parse(final String string) {

        Pattern pattern = Pattern.compile(Constant.WORD);
        Matcher matcher = pattern.matcher(string);

        List<Component> components = new ArrayList<>();
        while (matcher.find()) {
            String word = matcher.group();
            components.add(new Composite(TextType.WORD, word));
        }
        LOG_INFO.log(Level.INFO, "Parse WORD completed successfully");
        return components;
    }
}
