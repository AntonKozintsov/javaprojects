package com.epam.by.specification.parse;

import com.epam.by.composite.Component;
import com.epam.by.composite.Leaf;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse symbol.
 */
public final class ParseSymbol {
    /**
     * private Constructor.
     */
    private ParseSymbol() {
    }
    /**
     * to log info.
     */
    private static final Logger LOG_INFO = LogManager.
            getLogger(ParseSymbol.class.getName());

    /**
     * @param string string
     * @return List of components.
     */
    public static List<Component> parse(final String string) {

        final List<Component> components = new ArrayList<>();
        final char[] symbols = string.toCharArray();
        char symbol;

        for (char symbol1 : symbols) {
            symbol = symbol1;
            components.add(new Leaf(symbol));
        }
        LOG_INFO.log(Level.INFO, "Parse SYMBOL completed successfully");
        return components;
    }
}
