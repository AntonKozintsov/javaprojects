package com.epam.by.test;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.specification.parse.ParseSentence;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

/**
 * Parse sentence test.
 */
public class ParseSentenceTest {
    /**
     * @return data
     */
    @DataProvider(name = "sentenceData")
    public Object[][] parseSentence() {
        return new Object[][] {{new ArrayList<Component>() {
            {

                this.add(new Composite(TextType.SENTENCE, "Hello world."));
                this.add(new Composite(TextType.SENTENCE, "How are you?"));
                this.add(new Composite(TextType.SENTENCE, "My name's Nikita."));

            }
        }, "Hello world. How are you? My name's Nikita."}};
    }

    /**
     * @param component component
     * @param result result
     */
    @Test(dataProvider = "sentenceData")
    public void testParse(final ArrayList<Component> component, final String result) {

        Assert.assertEquals(ParseSentence.parse(result), component);

    }
}