package com.epam.by.test;

import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.read.ReadFile;
import org.testng.annotations.Test;

import java.io.IOException;


public class CompositeTest {
    /**
     * @throws IOException exception.
     */
    @Test
    public void testOperation() throws IOException {
        String text = ReadFile.readFile();
        Composite composite = new Composite(TextType.TEXT, text);
        composite.operation();
    }
}
