package com.epam.by.test;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.specification.parse.ParseExpression;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test class.
 */
public class ParseExpressionTest {
    /**
     * @return data
     */
    @DataProvider(name = "parseExpression")
    public Object[][] parseExpression() {
        return new Object[][] {{new ArrayList<Component>() {
            {
                this.add(new Composite(TextType.EXPRESSION, "9"));
            }
        }, "~6&9|(3&4)"}};

    }
    /**
     * @param result result
     * @param string string
     */
    @Test(dataProvider = "parseExpression")
    public void testParse(final List<Component> result,
                                      final String string) {


        Assert.assertEquals(result, ParseExpression.parse(string));

    }
}