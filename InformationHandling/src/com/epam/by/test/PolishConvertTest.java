package com.epam.by.test;

import com.epam.by.specification.convert.PolishConvert;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Convert test.
 */
public class PolishConvertTest {
    /**
     * @return data
     */
    @DataProvider(name = "polishConvert")
    public Object[][] polishConvert() {
        String expression = "5|(1&2&(3|(4&(3^5|6&47)|3)|2)|1)";
        int calculatedExpression =
                5 | (1 & 2 & (3 | (4 & (3 ^5 | 6 & 47) | 3) | 2) | 1);
        return new Object[][]{{expression,
                Integer.toString(calculatedExpression)}};
    }

    /**
     * @param expression expression.
     * @param calculatedExpression calculated expression.
     */
    @Test(dataProvider = "polishConvert")
    public void testCalculate(final String expression,
                              final String calculatedExpression) {
        Assert.assertEquals(PolishConvert.calculate(PolishConvert.
                convertToPolishNotation(expression)), calculatedExpression);
    }
}
