package com.epam.by.test;

import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.specification.sort.SortSentenceByWord;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Test.
 */
public class SortSentenceByWordTest {
    /**
     * @return data
     */
    @DataProvider(name = "sortSentence")
    public Object[][] sortSentence() {
        String text = "Hello my name is Anton.";
        String sortedText = "my is name Hello Anton.";
        Composite sortedComposite = new Composite(
                TextType.SENTENCE, sortedText);
        Composite composite = new Composite(TextType.SENTENCE, text);
        return new Object[][]{{sortedComposite, composite}};
    }

    /**
     * @param sortedComposite composite
     * @param composite composite
     */
    @Test(dataProvider = "sortSentence")
    public void testSortSentenceByWord(final Composite sortedComposite,
                                       final Composite composite) {

        Assert.assertEquals(sortedComposite, SortSentenceByWord.
                sortSentenceByWord(composite));
    }
}
