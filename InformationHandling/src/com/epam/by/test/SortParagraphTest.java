package com.epam.by.test;

import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.read.ReadFile;
import com.epam.by.specification.sort.SortParagraph;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class SortParagraphTest {
    @DataProvider(name = "sortParagraph")
    public Object[][] sortParagraph() throws IOException {
        String text = ReadFile.readFile();
        Composite composite = new Composite(TextType.TEXT, text);
        Composite sortedComposite = SortParagraph.sortParagraph(composite);
        return new Object[][]{{composite, sortedComposite}};
    }


    @Test(dataProvider = "sortParagraph")
    public void testSortParagraph(Composite composite, Composite sortedComposite) {
        Assert.assertEquals(SortParagraph.sortParagraph(composite),sortedComposite);
    }
}