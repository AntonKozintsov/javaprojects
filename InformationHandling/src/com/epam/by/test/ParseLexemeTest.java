package com.epam.by.test;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.specification.parse.ParseLexeme;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Lexeme parser test.
 */
public class ParseLexemeTest {
    /**
     * @return data
     */
    @DataProvider(name = "parseLexeme")
    public Object[][] parseLexeme() {

        return new Object[][] {{new ArrayList<Component>() {
            {

                this.add(new Composite(TextType.LEXEME, "Hello"));
                this.add(new Composite(TextType.LEXEME, "world"));
                this.add(new Composite(TextType.LEXEME, "test."));

            }
        }, "Hello world test."}};

    }

    /**
     * @param result result
     * @param string string
     */
    @Test(dataProvider = "parseLexeme")
    public void testParse(final List<Component> result,
                          final String string) {


        Assert.assertEquals(result, ParseLexeme.parse(string));

    }
}