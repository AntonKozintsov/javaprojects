package com.epam.by.test;

import com.epam.by.composite.Component;
import com.epam.by.composite.Composite;
import com.epam.by.constant.TextType;
import com.epam.by.specification.parse.ParseWord;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test.
 */
public class ParseWordTest {
    /**
     * @return data.
     */
    @DataProvider(name = "parseWord")
    public Object[][] parseWord() {

        return new Object[][] {{new ArrayList<Component>() {
            {

                this.add(new Composite(TextType.WORD, "Anton"));

            }
        }, "Anton."}};

    }

    /**
     * @param result result
     * @param string string
     */
    @Test(dataProvider = "parseWord")
    public void testParse(final List<Component> result,
                                final String string) {
        Assert.assertEquals(result, ParseWord.parse(string));

    }
}